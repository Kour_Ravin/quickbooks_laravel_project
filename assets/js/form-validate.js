$(document).ready(function ()
{
  $.validator.addMethod("number",
         function(value, element) {
             return /^(\d+(?:[\.\,]\d{2})?)$/.test(value);
         }, "Please Enter Number");


  $('.two-decimals').keypress(function(event) {  
        var $this = $(this);
        if ((event.which != 46 || $this.val().indexOf('.') != -1) && ((event.which < 48 || event.which > 57) && (event.which != 0 && event.which != 8))) {
            event.preventDefault();
        }

        var text = $(this).val();
        if ((event.which == 46) && (text.indexOf('.') == -1)) {
            setTimeout(function() {
                if ($this.val().substring($this.val().indexOf('.')).length > 3) {
                    $this.val($this.val().substring(0, $this.val().indexOf('.') + 3));
                }
            }, 1);
        }

        if ((text.indexOf('.') != -1) && (text.substring(text.indexOf('.')).length > 2) && (event.which != 0 && event.which != 8) && ($(this)[0].selectionStart >= text.length - 2)) {
            event.preventDefault();
        }
    });
    
    $('.two-decimals').bind("paste", function(e) {
        var text = e.originalEvent.clipboardData.getData('Text');
        if ($.isNumeric(text)) {
            if ((text.substring(text.indexOf('.')).length > 3) && (text.indexOf('.') > -1)) {
                e.preventDefault();
                $(this).val(text.substring(0, text.indexOf('.') + 3));
            }
        }
        else {
            e.preventDefault();
        }
    });
  
    $('.only-number').keypress(function(event) {  
        var $this = $(this);
        if ( ((event.which < 48 || event.which > 57) && (event.which != 0 && event.which != 8))) {
            event.preventDefault();
        }
    });

    $('.only-integer').keypress(function(event) {  
        var $this = $(this);
        if ( (event.which != 45 || $this.val().indexOf('-') != -1) && ((event.which < 48 || event.which > 57) && (event.which != 0 && event.which != 8))) {
            event.preventDefault();
        }
    });
  

    /*====================Start login form validation================= */
    $("#login-form").validate({
        errorClass   : "has-error",
        highlight    : function(element, errorClass)
        {
            $(element).parent('.input-group').addClass("has-error");
        },
        unhighlight  : function(element, errorClass, validClass)
        {
            $(element).parent('.input-group').removeClass("has-error");
        },
        errorPlacement: function (error, element)
        {
            error.insertAfter($(element).parent('.input-group'));
        },
        rules:
        {
            email: {
                required: true,
                email: true
            },
            password:{
                required: true,
                minlength: 6
            },
        },
        messages:
        {
            email: {
                required: "Please enter your email",
                email: "Please enter valid email address"
            },
            password: {
                required: "Please enter your password",
                minlength: "Your password must be at least 6 characters long"
            },
        },
        submitHandler: function (form)
        {
            formSubmit(form);
        }
    });
    /*====================End login form validation================= */

    /*====================Start Registration form validation================= */
    $("#register-form").validate({
        rules:
        {
            first_name: {
                required: true,
            },
            last_name: {
                required: true,
            },
            email: {
                required: true,
                email: true
            },
            user_name: {
                required: true,
            },
            password:{
                required: true,
                minlength: 6
            },
            password_confirmation:{
                required: true,
                minlength: 6,
                equalTo: "#password"
            },
        },
        messages:
        {
            first_name: {
                required: "Please enter first name"
            },
            last_name: {
                required: "Please enter last name"
            },
            email: {
                required: "Please enter email",
                email: "Please enter valid email address"
            },
            user_name: {
                required: "Please enter user name"
            },
            password: {
                required: "Please enter password",
                minlength: "Your password must be at least 6 characters long"
            },
            password_confirmation: {
                required:  "Please enter confirm password",
                minlength: "Your password must be at least 6 characters long",
                equalTo:   "Please enter the same value again."
            },
        },
        submitHandler: function (form)
        {
            formSubmit(form);
        }
    });
    /*====================End Registration form validation================= */
   
    /*====================Add Customer Form================= */
    $("#customer-form").validate({
        rules:
        {
            first_name: {
                required: true,
            },
            last_name: {
                required: true,
            },
            email: {
                required: true,
                email: true
            },
            nick_name: {
                required: true,
            },
            register_shipping_rate: {
                required: true,
                //number:true,
            },
            // 'sg_shipping_rate[]': {
            //     required: true,
            //     //number:true,
            // }
        },
        messages:
        {
            first_name: {
                required: "Please enter first name"
            },
            last_name: {
                required: "Please enter last name"
            },
            email: {
                required: "Please enter email",
                email: "Please enter valid email address"
            },
            nick_name: {
                required: "Please enter nick name"
            },
            register_shipping_rate: {
                required: "Please enter register shipping rate",
                number:"Please enter a valid number"
            },
            // 'sg_shipping_rate[]': {
            //     required: "Please enter register shipping rate",
            //     number:"Please enter a valid number"
            // }
        },
        submitHandler: function (form)
        {
            formSubmit(form);
        }
    });
   
    $(".sg_shipping_rate").each(function()
    {
        $(this).rules('remove');
        $(this).rules('add', {
            required: true,
            messages: {
                required: "Please enter shipping rate."
            },
        });
    });

    $(".topills").each(function()
    {
        $(this).rules('remove');
        $(this).rules('add', {
            required: true,
            messages: {
                required: "Please enter to pills."
            },
        });
    });
    /*====================Add Customer Form================= */

     /*====================Order Mapping================= */
    $("#ordermapping-form").validate({

        rules:
        {
            client_name: {
                required: true,
            },
            city: {
                required: true,
            },
            // state: {
            //     required: true,
            // },
            zip: {
                required: true,
            },
            country: {
                required: true,
            },
            address1: {
                required: true,
            },
            // sku: {
            //     required: "#is_sku_required:checked"
            // },
            sku: {
                required: true,
            },
            product: {
                required: true,
            },
            quantity: {
                required: true,
            },
            order_id: {
                required: true,
            },
            shipping_method: {
                //required: true,
                required: "#is_shipping_method_required:checked"
            }
        },
        messages:
        {
            client_name: {
                required: "Select Field For Client Name"
            },
            city: {
                required: "Select Field For City"
            },
            // state: {
            //     required: "Select Field For State"
            // },
            zip: {
                required: "Select Field For Zip Code"
            },
            country: {
                required: "Select Field For Country"
            },
            address: {
                required: "Select Field For Address"
            },
            sku: {
                required: "Select Field For SKU"
            },
            product: {
                required: "Select Field For Product"
            },
            countity: {
                required: "Select Field For Quantity"
            },
            shipping_method: {
                required: "Select Field For Shipping Method"
            },
            order_id: {
                required: "Select Field For Order Id"
            }
        },
        submitHandler: function (form)
        {
            formSubmit(form);
        }
    });

    /*====================Order Mapping================= */
    $("#appealplace-form").validate({

        rules:
        {
            filename: {
                required: true,
            },

        },
        messages:
        {
            filename: {
                required: "File Name Is Required field"
            },

        },
        submitHandler: function (form)
        {
            formSubmit(form);
        }
    });

    /* ====================Add Product Form================= */
    $("#Product-form").validate({
        rules:
        {
            // 'supplier[]': {
            //     required: true,
            // },
            sku: {
                required: true,
            },
            product_name: {
                required: true,
            },
            stock: {
                required: true,
                number : true
            },
            expiry_date: {
                required: true,
            },
            brand_name:{
                required:true,
            },
            product_type:{
                required:true,
            }
        },
        messages:
        {
            'supplier[]': {
                required: "Please select supplier"
            },
            sku: {
                required: "Please enter sku"
            },
            product_name: {
                required: "Please enter product name",
            },
            cost_price: {
                required: "Please enter cost price",
            },
            brand_name:{
                required:"Please enter brand name",
            },
            expiry_date: {
                required: "Please enter expiry date",
            },
            stock: {
                required: "Please enter stock quantity"
            },
            product_type: {
                required: "Please enter product type"
            }        },
        submitHandler: function (form)
        {
            formSubmit(form);
        }
    });



    /* ====================Add Product Form================= */
    $("#Product-type-form").validate({
        rules:
        {            
            product_type:{
                required:true,
            },
            per_parcel_qty:{
                required:true,
            },
        },
        messages:
        {           
            product_type: {
                required: "Please enter product type"
            },
            per_parcel_qty: {
                required: "Please enter per parcel quantity"
            }
        },
        submitHandler: function (form)
        {
            formSubmit(form);
        }
    });


// valid cost price
  jQuery(document).on('keypress', '#cost_price', function (eve) {
    if (eve.which == 0) {
      return true;
    } else {
      if (eve.which == '.') {
        eve.preventDefault();
      }
      if ((eve.which != 46 || $(this).val().indexOf('.') != -1) && (eve.which < 48 || eve.which > 57)) {
        if (eve.which != 8)
        {
          eve.preventDefault();
        }      }
        $('#cost_price').keyup(function (eve) {
          if ($(this).val().indexOf('.') == 0) {
            $(this).val($(this).val().substring(1));
          }
        });
      }
    });
  //valid selling price
  jQuery(document).on('keypress', '#selling_price', function (eve) {
    if (eve.which == 0) {
      return true;
    } else {
      if (eve.which == '.') {/*====================Start login form validation================= */
    $("#login-form").validate({
        errorClass: "has-error",
        highlight: function (element, errorClass) {
            $(element).parents('.form-group').addClass(errorClass);
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).parents('.form-group').removeClass(errorClass);
        },
        rules:
        {
            email: {
                required: true,
                email: true
            },
            password:{
                required: true,
                minlength: 6
            },
        },
        messages:
        {
            email: {
                required: "Please enter your email",
                email: "Please enter valid email address"
            },
            password: {
                required: "Please enter your password",
                minlength: "Your password must be at least 6 characters long"
            },
        },
        submitHandler: function (form)
        {
            formSubmit(form);
        }
    });
    /*====================End login form validation================= */
        eve.preventDefault();
      }
      if ((eve.which != 46 || $(this).val().indexOf('.') != -1) && (eve.which < 48 || eve.which > 57)) {
        if (eve.which != 8)
        {
          eve.preventDefault();
        }      }
        $('#selling_price').keyup(function (eve) {
          if ($(this).val().indexOf('.') == 0) {
            $(this).val($(this).val().substring(1));
          }
        });
      }
    });
  //for valid amount
    jQuery(document).on('keypress', '#amount', function (eve) {
    if (eve.which == 0) {
      return true;
    } else {
      if (eve.which == '.') {
        eve.preventDefault();
      }
      if ((eve.which != 46 || $(this).val().indexOf('.') != -1) && (eve.which < 48 || eve.which > 57)) {
        if (eve.which != 8)
        {
          eve.preventDefault();
        }      }
        $('#amount').keyup(function (eve) {
          if ($(this).val().indexOf('.') == 0) {
            $(this).val($(this).val().substring(1));
          }
        });
      }
    });

    /*====================End product form validation================= */
    /*====================Add Customer Form================= */

    //forgot Password
    $("#forgotPassword-form").validate({
        errorClass   : "has-error",
        highlight    : function(element, errorClass) {
            $(element).parents('.form-group').addClass(errorClass);
        },
        unhighlight  : function(element, errorClass, validClass) {
            $(element).parents('.form-group').removeClass(errorClass);
        },
        rules:
        {
            email:
            {
                required: true,
                email: true
            },
        },
        messages:
        {
            email: {
                required: "Please enter your email",
                email: "Please enter valid email address"
            },
        },
        submitHandler: function (form)
        {
            formSubmit(form);
        }
    });

    $('#passForm').validate({
      rules: {
        password: {
          required: true,
          minlength: 6
        },
        confirm_password: {
          required: true,
          minlength: 6,
          equalTo: "#password"
        },
      },
      messages: {
        password: {
          required: "Please enter your password",
          minlength:    "Your password must be at least 6 characters long"
        },
        confirm_password: {
          required:     "Please enter your confirm password",
          minlength:    "Your password must be at least 6 characters long",
          equalTo:  "Please enter the same password as above"
        },
      },

      submitHandler: function (form)
      {
        formSubmit(form);
      }
    });

    $('.resetpassword').validate({
        rules:  {
            email:  {
                required: true,
                email: true
            },
            password:   {
                required: true,
                minlength: 6
            },
            password_confirmation:  {
                required: true,
                minlength: 6,
                equalTo: "#password"
            },
        },
        messages:   {
            email:  {
                required: "Please enter your email",
                email: "Please enter valid email address"
            },
            password:   {
                required: "Please enter your password",
                minlength:  "Your password must be at least 6 characters long"
            },
            password_confirmation:  {
                required:   "Please enter your confirm password",
                minlength:  "Your password must be at least 6 characters long",
                equalTo:  "Please enter the same password as above"
            },
        },
        submitHandler: function (form)
        {
            formSubmit(form);
        }
    });



    $(".reset-password").validate({
        errorClass   : "has-error",
        highlight    : function(element, errorClass)
        {
            $(element).parent('.input-group').addClass("has-error");
        },
        unhighlight  : function(element, errorClass, validClass)
        {
            $(element).parent('.input-group').removeClass("has-error");
        },
        errorPlacement: function (error, element)
        {
            error.insertAfter($(element).parent('.input-group'));
        },
        rules:
        {
            email: {
                required: true,
                email: true
            },
        },
        messages:
        {
            email: {
                required: "Please enter your email",
                email: "Please enter valid email address"
            },
        },
        submitHandler: function (form)
        {
            formSubmit(form);
        }
    });

    //change Password-form
    $("#changePassword-form").validate({
        errorClass: "has-error",
        highlight: function (element, errorClass) {
            $(element).parents('.form-group').addClass(errorClass);
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).parents('.form-group').removeClass(errorClass);
        },
        rules:
        {
            password:
            {
                required: true,
            },
            new_password:
            {
                required: true,
                minlength: 6,
                // equalTo: '#confirm_password'
            },
            confirm_password:
            {
                required: true,
                minlength: 6,
                equalTo: '#new_password'
            },
        },
        messages:
        {
            password: "Old Password is required.",
            new_password:
            {
                'required': "New Password is required.",
                'minlength': "New Password must contain at least 6 characters.",
                //'equalTo': "New Password and Confirm Password not mactched."
            },
            confirm_password:
            {
                'required': "Confirm Password is required.",
                'minlength': "Confirm Password must contain at least 6 characters.",
                'equalTo': "New Password and Confirm Password not mactched."
            },
        },

        submitHandler: function (form)
        {
            formSubmit(form);
        }
    });


    //Product excel upload
    $("#product_upload_excel").validate({
        errorClass: "has-error",
        highlight: function (element, errorClass) {
            $(element).parents('.form-group').addClass(errorClass);
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).parents('.form-group').removeClass(errorClass);
        },
        rules:
        {
            productsFile:
            {
                required: true,
            },
        },
        messages:
        {
            productsFile: "Please choose file...",
        },
        submitHandler: function (form)
        {
            formSubmit(form);
        }
    });

    //Order excel upload
    $("#update_tracking_excel").validate({
        errorClass: "has-error",
        highlight: function (element, errorClass) {
            $(element).parents('.form-group').addClass(errorClass);
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).parents('.form-group').removeClass(errorClass);
        },
        rules:
        {
            update_trackking_number:
            {
                required: true,
            },
        },
        messages:
        {
            update_trackking_number: "Please choose file...",
        },
        submitHandler: function (form)
        {
            formSubmit(form);
        }
    });

    //customer price form
    $("#customer_price_form").validate({
        errorClass: "has-error",
        highlight: function (element, errorClass) {
            $(element).parents('.form-group').addClass(errorClass);
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).parents('.form-group').removeClass(errorClass);
        },
        rules:
        {
            customer_price_file:
            {
                required: true,
            },
        },
        messages:
        {
            customer_price_file: "Please choose file...",
        },
        submitHandler: function (form)
        {
            formSubmit(form);
        }
    });

    //supplier price form
    $("#supplier_price_form").validate({
        errorClass: "has-error",
        highlight: function (element, errorClass) {
            $(element).parents('.form-group').addClass(errorClass);
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).parents('.form-group').removeClass(errorClass);
        },
        rules:
        {
            supplier_price_file:
            {
                required: true,
            },
        },
        messages:
        {
            supplier_price_file: "Please choose file...",
        },
        submitHandler: function (form)
        {
            formSubmit(form);
        }
    });

    //supplier price form
    $("#supplier_price_form").validate({
        errorClass: "has-error",
        highlight: function (element, errorClass) {
            $(element).parents('.form-group').addClass(errorClass);
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).parents('.form-group').removeClass(errorClass);
        },
        rules:
        {
            orderexcelFile:
            {
                required: true,
            },
        },
        messages:
        {
            orderexcelFile: "Please choose file...",
        },
        submitHandler: function (form)
        {
            formSubmit(form);
        }
    });

    //Assign Supplier
    $("#assignSupplier-form").validate({
        errorClass: "has-error",
        highlight: function (element, errorClass) {
            $(element).parents('.form-group').addClass(errorClass);
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).parents('.form-group').removeClass(errorClass);
        },
        rules:
        {
            supplier:
            {
              required: true,
            },
            cus_shipping_rate:
            {
              required: true,
              number : true
            },
            sup_shipping_rate:
            {
              required: true,
              number : true
            }
        },
        messages:
        {
            supplier:
             {
              required:"Please select one Supplier",
             },
            cus_shipping_rate:
            {
              required: "Please select Customer Shipping Rate",
              number: "Please enter a valid Number"
            },
            sup_shipping_rate:
            {
              required: "Please select Supplier Shipping Rate",
              number: "Please enter  a valid Number"
            }
        },

        submitHandler: function (form)
        {
            formSubmit(form);
        }
    });

    //Redirect Order To Another Supplier
    $("#redirectOrder-form").validate({        
        rules:
        {
            supplier: {
                required: true,
            },
            customer_shipping: {
                required: true,
            },
        },
        messages:
        {
            supplier: {
                required: "Please select supplier to redirect"
            },
            customer_shipping: {
                required: "Please customer shipping amount"
            },
        },
        submitHandler: function (form)
        {
            formSubmit(form);
        }
    });

    $(".redirect_price").each(function()
    {
        $(this).rules('remove');
        $(this).rules('add', {
            required: true,
            messages: {
                required: "Please enter Price."
            },
        });
    });

    //Assign Tracking
    $("#assignTracking-form").validate({
        errorClass: "has-error",
        highlight: function (element, errorClass) {
            $(element).parents('.form-group').addClass(errorClass);
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).parents('.form-group').removeClass(errorClass);
        },
        rules:
        {
            tracking_number:
            {
                required: true,
            },
        },
        messages:
        {
            tracking_number: "Please Insert Tracking Number",
        },

        submitHandler: function (form)
        {
            formSubmit(form);
        }
    });


    /*//Full Pay Form
    $("#fullPay-form").validate({
        errorClass: "has-error",
        highlight: function (element, errorClass) {
            $(element).parents('.form-group').addClass(errorClass);
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).parents('.form-group').removeClass(errorClass);
        },
        rules:
        {
            userid:
            {
                required: true,
            },
            amount:
            {
                required: true,
            },
        },
        messages:
        {
            userid:
             {
              required:"Supplier/Customer id is required.",
             },
             amount:
             {
                required:"Amount field is required.",
             },
        },

        submitHandler: function (form)
        {
            formSubmit(form);
        }
    });


     //Order Pay Form
    $("#orderPay-form").validate({
        errorClass: "has-error",
        highlight: function (element, errorClass) {
            $(element).parents('.form-group').addClass(errorClass);
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).parents('.form-group').removeClass(errorClass);
        },
        rules:
        {
            orderid:
            {
                required: true,
            },
            actualamt123:
            {
                required: true,
            },
            remainingamt:
            {
                required: true,
            },
        },
        messages:
        {
            orderid:
            {
                required:"Order id field is required.",
            },
            actualamt:
            {
                required:"Actual amount field is required.",
            },
            remainingamt:
            {
                required:"Remaining amount field is required.",
            },
        },

        submitHandler: function (form)
        {
            formSubmit(form);
        }
    });*/


    /*====================Dashboard filter form validation================= */
    $("#dashboard-filter").validate({
        rules:
        {
            dashboard_start_date: {
                required: true
            },
            dashboard_end_date:{
                required: true
            },
        },
        messages:
        {
            dashboard_start_date: {
                required: "Start date is required."
            },
            dashboard_end_date: {
                required: "End date is required."
            },
        },
        submitHandler: function (form)
        {
            formSubmit(form);
        }
    });
    /*====================Dashboard filter form validation================= */

});

function formSubmit(form)
{
    $.ajax({
        url         : form.action,
        type        : form.method,
        data        : new FormData(form),
        contentType : false,
        cache       : false,
        headers     : {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        processData : false,
        dataType    : "json",
        beforeSend  : function () {
            $(".button-disabled").attr("disabled", "disabled");
            $("#loader-section").css('display','block');
        },
        complete: function () {
            $("#loader-section").css('display','none');
        },
        success: function (response) {

            $.toast().reset('all');
            var delayTime = 3000;
            if(response.delayTime)
                delayTime = response.delayTime;

            if (response.warning)
            {
                $.toast({
                    heading             : 'Warning',
                    text                : response.warning_message,
                    loader              : true,
                    loaderBg            : '#fff',
                    showHideTransition  : 'fade',
                    icon                : 'warning',
                    hideAfter           : delayTime,
                    position            : 'top-right'
                });
            }else if (response.success)
            {
                $.toast({
                    heading             : 'Success',
                    text                : response.success_message,
                    loader              : true,
                    loaderBg            : '#fff',
                    showHideTransition  : 'fade',
                    icon                : 'success',
                    hideAfter           : delayTime,
                    position            : 'top-right'
                });
            }
            else
            {
                $(".button-disabled").removeAttr("disabled");
                if( response.formErrors)
                {
                  //console.log(response.errors);
                    $.toast({
                        heading             : 'Error',
                        text                : response.errors,
                        loader              : true,
                        loaderBg            : '#fff',
                        showHideTransition  : 'fade',
                        icon                : 'error',
                        hideAfter           : delayTime,
                        position            : 'top-right'
                    });
                    /* $.each(response.errors, function( index, value )
                    {
                        $.toast({
                            heading             : 'Error',
                            text                : value,
                            loader              : true,
                            loaderBg            : '#fff',
                            showHideTransition  : 'fade',
                            icon                : 'error',
                            hideAfter           : delayTime,
                            position            : 'top-right'
                        });
                    }); */
            }
            else
            {
                jQuery('#InputEmail').val(' ');
                if(response.alert_message){
                    $(".button-disabled").removeAttr("disabled");
                    $('#update_trackking_number').val('');
                    $('#Upload_Tracking_Number').modal('toggle');  
                    $('#alert_msg_div').show();
                    $('#alert_msg_p').html(response.alert_message);
                    // $.toast({
                    //     heading             : 'Error',
                    //     text                : response.error_message,
                    //     loader              : true,
                    //     loaderBg            : '#fff',
                    //     showHideTransition  : 'fade',
                    //     icon                : 'error',
                    //     hideAfter           : delayTime,
                    //     position            : 'top-right'
                    // });    
                }else{
                    $.toast({
                        heading             : 'Error',
                        text                : response.error_message,
                        loader              : true,
                        loaderBg            : '#fff',
                        showHideTransition  : 'fade',
                        icon                : 'error',
                        hideAfter           : delayTime,
                        position            : 'top-right'
                    });    
                }
            }
        }
    if(response.ajaxPageCallBack)
    {
        response.formid = form.id;
        ajaxPageCallBack(response);
    }
    if(response.resetform)
        $(form.id).resetForm();
    if(response.url && response.url != '')
    {
        if(response.delayTime){
            //setTimeout(function() { window.location.href=response.url;}, response.delayTime);
            setTimeout(function() { location.replace(response.url);}, response.delayTime);
        }
        else{
            //window.location.href=response.url;
            location.replace(response.url);
        }

    }else{
      if(response.html)
      {
        jQuery("#allResult").html(response.html);
      }
      else {
        // setTimeout(function(){
        //   location.reload();
        // }, 2000);
      }
    }


    // if(response.url && response.url != '')
    // {
    //     if(response.delayTime)
    //         setTimeout(function() { window.location.href=response.url;}, response.delayTime);
    //     else
    //         window.location.href=response.url;
    // }else{
    //   if(response.html)
    //   {
    //     jQuery("#allResult").html(response.html);
    //   }
    //   else {
    //     // setTimeout(function(){
    //     //   location.reload();
    //     // }, 2000);
    //   }
    // }
},
error:function(response){
    console.log(response);
    jQuery.each(response.responseJSON.errors,function(k,message){
        $.toast({
            heading             : 'Error',
            text                : message,
            loader              : true,
            loaderBg            : '#fff',
            showHideTransition  : 'fade',
            icon                : 'error',
            hideAfter           : 4000,
            position            : 'top-right'
        });
    });
}
});
}
