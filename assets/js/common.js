jQuery(document).ready(function(){
    $(document).on('click','.getUserID',function(){   
        var userid    = jQuery(this).attr('data-id');  
        var url       = jQuery(this).attr('data-ref'); 
        jQuery.ajax({
            type: "post",           
            url: base_url+'/set-userid-session',
            data : {userid : userid},
            dataType: "json",
            success: function (msg) {
            	if(msg.success_message == 'yes')
            	{
            		window.location.href = url;
            	}
            	else
            	{
            		alert('Please refresh this page again.');
            	}          		
        	}
        });
    });
    $(document).on('click','.diconnectUser',function(){   
        var userid    = jQuery(this).attr('data-id'); 
        jQuery.ajax({
            type: "post",           
            url: base_url+'/qb-user-disconnect',
            data : {userid : userid},           
            success: function (msg) {                
                window.location.href = base_url+'/user';   
                //window.location =   base_url+'/user';          
            }
        });
    });
});


