$(document).ready( function() {

    //$(".two-decimals").change(function(){
    // $(document).on('blur','.two-decimals',function(event) {
    //    this.value = parseFloat(this.value).toFixed(2);
    // });

    $(document).on('click','.user_modal',function(event) {
        event.preventDefault();
        var getAction = $(this).attr("data-action");
        var getTitle = $(this).attr("data-title");
        var getUrl = $(this).attr("data-url");
        if(getAction == 'edit'){
            var getId = $(this).attr("data-id");
            dataString = {action : getAction, id : getId, title : getTitle};
            getTitle = "Edit "+getTitle;
        }else{
            dataString = {action : getAction, title : getTitle};
            getTitle = "Add "+getTitle;
        }

        $.ajax({
            url         : getUrl,
            type        : 'POST',
            data        :  dataString,
            headers     : {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            dataType    : "html",
            beforeSend  : function () {
                //$("#loader-section").css('display','block');
                //return false;
            },
            complete: function () {
                //$("#loader-section").css('display','none');
            },
            success: function (response) {

                $(".user-modal-title").html(getTitle);
               // $("login").html(getTitle);

                $(".user-modal-body").html(response);

            },
            error:function(response){
                // console.log(response);
                // console.log('sdsdsds');
            }
        });
    });

    $('body').on('click','.filter',function(event) {
        event.preventDefault();
        var getAction = $(this).attr("action");
        var formdata = $(this).serialize();

        $.ajax({
            url         : getAction+'/dashboard_filter',
            type        : 'POST',
            data        :  formdata,
            headers     : {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            dataType    : "html",
            beforeSend  : function () {
                //$("#loader-section").css('display','block');
                //return false;
            },
            complete: function () {
                //$("#loader-section").css('display','none');
            },
            success: function (response) {
                jQuery(".filterAdmin").html(response);
            },
            error:function(response){
                // console.log(response);
                // console.log('sdsdsds');
            }
        });
    });

    $('body').on('click','.sup_modal',function(event) {
        event.preventDefault();
        var getAction = $(this).attr("data-action");
        var getTitle = $(this).attr("data-title");
        var getUrl = $(this).attr("data-url");
        if(getAction == 'edit'){
            var getId = $(this).attr("data-id");
            dataString = {action : getAction, id : getId, title : getTitle};
        }else{
            dataString = {action : getAction, title : getTitle};
        }

        $.ajax({
            url         : getUrl,
            type        : 'POST',
            data        :  dataString,
            headers     : {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            dataType    : "html",
            beforeSend  : function () {
                //$("#loader-section").css('display','block');
                //return false;
            },
            complete: function () {
                //$("#loader-section").css('display','none');
            },
            success: function (response) {

                $(".user-modal-title").html(getTitle);

                $(".user-modal-body").html(response);

            },
            error:function(response){
                // console.log(response);
                // console.log('sdsdsds');
            }
        });
    });

    $('body').on('click','.cus_modal',function(event) {
        event.preventDefault();
        var getAction = $(this).attr("data-action");
        var getTitle = $(this).attr("data-title");
        var getUrl = $(this).attr("data-url");
        if(getAction == 'edit'){
            var getId = $(this).attr("data-id");
            dataString = {action : getAction, id : getId, title : getTitle};
        }else{
            dataString = {action : getAction, title : getTitle};
        }

        $.ajax({
            url         : getUrl,
            type        : 'POST',
            data        :  dataString,
            headers     : {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            dataType    : "html",
            beforeSend  : function () {
                //$("#loader-section").css('display','block');
                //return false;
            },
            complete: function () {
                //$("#loader-section").css('display','none');
            },
            success: function (response) {

                $(".user-modal-title").html(getTitle);
                $(".user-modal-body").html(response);

            },
            error:function(response){
                // console.log(response);
                // console.log('sdsdsds');
            }
        });
    });

    $('body').on('click','.emp_modal',function(event) {
        event.preventDefault();
        var getAction = $(this).attr("data-action");
        var getTitle = $(this).attr("data-title");
        var getUrl = $(this).attr("data-url");
        if(getAction == 'edit'){
            var getId = $(this).attr("data-id");
            dataString = {action : getAction, id : getId, title : getTitle};
        }else{
            dataString = {action : getAction, title : getTitle};
        }

        $.ajax({
            url         : getUrl,
            type        : 'POST',
            data        :  dataString,
            headers     : {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            dataType    : "html",
            beforeSend  : function () {
                //$("#loader-section").css('display','block');
                //return false;
            },
            complete: function () {
                //$("#loader-section").css('display','none');
            },
            success: function (response) {

                $(".user-modal-title").html(getTitle);

                $(".user-modal-body").html(response);
                console.log(response);
            },
            error:function(response){
                // console.log(response);
                // console.log('sdsdsds');
            }
        });
    });

    jQuery('body').on('change', '.changeStatus', function (event){
        event.preventDefault();
        var id = jQuery(this).attr('data-id');
        var value = jQuery(this).attr('rel');
        var type = jQuery(this).attr('ref');
        var url = base_url+'/changeuserstatus';

        if(value == 1){
            jQuery(this).attr('rel',0);
        }
        else{
            jQuery(this).attr('rel',1);
        }

        var data = {
          value:value,
          id:id,
          type:type
        };

        $.ajax({
            url         : url,
            type        : 'POST',
            data        :  data,
            headers     : {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            dataType    : "json",
            beforeSend  : function () {
                //$("#loader-section").css('display','block');
                //return false;
            },
            complete: function () {
                //$("#loader-section").css('display','none');
            },
            success: function (response) {
                $.toast().reset('all');
                var delayTime = 3000;
                if (response.success)
                {
                    $.toast({
                        heading             : 'Success',
                        text                : response.success_message,
                        loader              : true,
                        loaderBg            : '#fff',
                        showHideTransition  : 'fade',
                        icon                : 'success',
                        hideAfter           : delayTime,
                        position            : 'top-right'
                    });
                }
                else
                {
                    $.toast({
                        heading             : 'Error',
                        text                : response.error_message,
                        loader              : true,
                        loaderBg            : '#fff',
                        showHideTransition  : 'fade',
                        icon                : 'error',
                        hideAfter           : delayTime,
                        position            : 'top-right'
                    });
                }
            },
            error:function(response){

            }
        });
    });

    //Accept Paymet Method    
    jQuery('body').on('change', '.orderAcceptPayment', function (event){
        event.preventDefault();
        var id = jQuery(this).attr('data-id');
        var rowindex = jQuery(this).attr('data-rowindex');
        var url = base_url+'/payment-received';
        var currentThis = jQuery(this);

        $.ajax({
            url         : url,
            type        : 'POST',
            data        : {id:id},
            headers     : {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            dataType    : "json",
            beforeSend  : function () {
                //$("#loader-section").css('display','block');
                //return false;
            },
            complete: function () {
                //$("#loader-section").css('display','none');
            },
            success: function (response) {
                currentThis.attr('disabled','');
                jQuery('.lable'+rowindex).attr('title','Payment Received.');
                $.toast().reset('all');
                var delayTime = 3000;
                if (response.success)
                {
                    $.toast({
                        heading             : 'Success',
                        text                : response.success_message,
                        loader              : true,
                        loaderBg            : '#fff',
                        showHideTransition  : 'fade',
                        icon                : 'success',
                        hideAfter           : delayTime,
                        position            : 'top-right'
                    });
                }
                else
                {
                    $.toast({
                        heading             : 'Error',
                        text                : response.error_message,
                        loader              : true,
                        loaderBg            : '#fff',
                        showHideTransition  : 'fade',
                        icon                : 'error',
                        hideAfter           : delayTime,
                        position            : 'top-right'
                    });
                }
            },
            error:function(response){

            }
        });
    });

    //change product status
    jQuery('body').on('change', '.changeproductStatus', function (event){
        event.preventDefault();
        var id = jQuery(this).attr('data-id');
        var value = jQuery(this).attr('rel');
        var type = jQuery(this).attr('ref');
        var url = base_url+'/changeproductStatus';

        if(value == 1){
            jQuery(this).attr('rel',0);
        }
        else{
            jQuery(this).attr('rel',1);
        }

        var data = {
          value:value,
          id:id,
          type:type
        };

        $.ajax({
            url         : url,
            type        : 'POST',
            data        :  data,
            headers     : {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            dataType    : "json",
            beforeSend  : function () {
                //$("#loader-section").css('display','block');
                //return false;
            },
            complete: function () {
                //$("#loader-section").css('display','none');
            },
            success: function (response) {
                $.toast().reset('all');
                var delayTime = 3000;
                if (response.success)
                {
                    $.toast({
                        heading             : 'Success',
                        text                : response.success_message,
                        loader              : true,
                        loaderBg            : '#fff',
                        showHideTransition  : 'fade',
                        icon                : 'success',
                        hideAfter           : delayTime,
                        position            : 'top-right'
                    });
                }
                else
                {
                    $.toast({
                        heading             : 'Error',
                        text                : response.error_message,
                        loader              : true,
                        loaderBg            : '#fff',
                        showHideTransition  : 'fade',
                        icon                : 'error',
                        hideAfter           : delayTime,
                        position            : 'top-right'
                    });
                }
            },
            error:function(response){

            }
        });
    });

    $('body').on('click','.pro_modal',function(event) {
        event.preventDefault();
        var getAction = $(this).attr("data-action");
        var getTitle = $(this).attr("data-title");
        var getUrl = $(this).attr("data-url");
        if(getAction == 'edit'){
            var getId = $(this).attr("data-id");
            //alert(getId);
            dataString = {action : getAction, id : getId, title : getTitle};
        }else{
            dataString = {action : getAction, title : getTitle};
        }

        $.ajax({
            url         : getUrl,
            type        : 'POST',
            data        :  dataString,
            headers     : {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            dataType    : "html",
            beforeSend  : function () {
                //$("#loader-section").css('display','block');
                //return false;
            },
            complete: function () {
                //$("#loader-section").css('display','none');
            },
            success: function (response) {

                $(".user-modal-title").html(getTitle);

                $(".user-modal-body").html(response);
            },
            error:function(response){
                // console.log(response);
                // console.log('sdsdsds');
            }
        });
    });



    $('body').on('click','.pro_type_modal',function(event) {
        event.preventDefault();
        var getAction = $(this).attr("data-action");
        var getTitle = $(this).attr("data-title");
        var getUrl = $(this).attr("data-url");
        if(getAction == 'edit'){
            var getId = $(this).attr("data-id");
            dataString = {action : getAction, id : getId, title : getTitle};
        }else{
            dataString = {action : getAction, title : getTitle};
        }

        $.ajax({
            url         : getUrl,
            type        : 'POST',
            data        :  dataString,
            headers     : {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            dataType    : "html",
            beforeSend  : function () {
                //$("#loader-section").css('display','block');
                //return false;
            },
            complete: function () {
                //$("#loader-section").css('display','none');
            },
            success: function (response) {
                $(".user-modal-title").html(getTitle);
                $(".user-modal-body").html(response);
            },
            error:function(response){
                // console.log(response);
                // console.log('sdsdsds');
            }
        });
    });


     var __item_ref = "";
     $('body').on('click','.delete-supplier-to-product',function(){
     var action = $('.delete').attr("data-action");
     if(action)
     {
         delete_single_id  = $(this).closest('.addsuprow').find('.product_supplier_id').val();
         if(delete_single_id != ''){
           __item_ref += delete_single_id+',';
              $('.deleteId').val(__item_ref);
         }

     }
    // var delete_supplier = base_url+'/delete_supplier/'+delete_id;
    // //`                                                                                                                                 alert(delete_supplier_url);
    //     jQuery.ajax({
    //     type: "GET",
    //     headers: {
    //       'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    //     },
    //    // data: {search:keyword,type:type,seg:seg,ajax: 1},
    //     url: delete_supplier,
    //     dataType: "json",
    //     success: function (response) {
    //       console.log(response);
    //         }
    //     });
    });



    // Order appeal detail view
    $('body').on('click','.view-appeal-details',function(event) {
        event.preventDefault();
        var appealId = $(this).attr("data-appealid");
        var getUrl = base_url+'/request-detail';

        $.ajax({
            url         : getUrl,
            type        : 'POST',
            data        : {appealId:appealId},
            headers     : {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            dataType    : "json",
            beforeSend  : function () {
                //$("#loader-section").css('display','block');
                //return false;
            },
            complete: function () {
                //$("#loader-section").css('display','none');
            },
            success: function (response) {
                jQuery("#appealDetail").html(response.html);                
                jQuery("#allResult").hide();
                jQuery("#appealOrderDetail").hide();
                jQuery("#appealDetail").show();
            },
            error:function(response){
                // console.log(response);
                // console.log('sdsdsds');
            }
        });
    });

    // Order appeal order detail view
    $('body').on('click','.view-appeal-order-details',function(event) {
        event.preventDefault();
        var orderId = $(this).attr("data-orderid");
        var getUrl = base_url+'/request-order-detail';

        $.ajax({
            url         : getUrl,
            type        : 'POST',
            data        : {orderId:orderId},
            headers     : {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            dataType    : "json",
            beforeSend  : function () {
                //$("#loader-section").css('display','block');
                //return false;
            },
            complete: function () {
                //$("#loader-section").css('display','none');
            },
            success: function (response) {
                jQuery("#allResult").hide();
                jQuery("#appealDetail").hide();
                jQuery("#appealOrderDetail").html(response.html);
                jQuery("#appealOrderDetail").show();
            },
            error:function(response){
            }
        });
    });

    // Order detail view
    $('body').on('click','.view-order-details',function(event) {
        event.preventDefault();
        var orderId = $(this).attr("data-orderid");
        var getUrl = base_url+'/order-detail';

        $.ajax({
            url         : getUrl,
            type        : 'POST',
            data        : {orderId:orderId},
            headers     : {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            dataType    : "json",
            beforeSend  : function () {
                //$("#loader-section").css('display','block');
                //return false;
            },
            complete: function () {
                //$("#loader-section").css('display','none');
            },
            success: function (response) {
                jQuery("#allResult").hide();
                jQuery("#detailResult").html(response.html);
                jQuery("#detailResult").show();
            },
            error:function(response){
            }
        });
    });

 // Order update detail view -- Ghanshyam
    $('body').on('click','.update-order-details',function(event) {
        event.preventDefault();
        var orderId = $(this).attr("data-orderid");
        var getUrl = base_url+'/edit-orders';
        $.ajax({
            url         : getUrl,
            type        : 'POST',
            data        : {orderId:orderId},
            headers     : {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            dataType    : "json",
            beforeSend  : function () {
                //$("#loader-section").css('display','block');
                //return false;
            },
            complete: function () {
                //$("#loader-section").css('display','none');
            },
            success: function (response) {
                jQuery("#allResult").hide();
                jQuery("#detailResult").html(response.html);
                jQuery("#detailResult").show();
            },
            error:function(response){
            }
        });
    });
// update order
$('body').on('click','.update-edited-order', function(event){
        event.preventDefault();
        var orderId = $(this).attr("data-orderid");
        var getUrl = base_url+'/edit-orders/update';
        $.ajax({
            url         : getUrl,
            type        : 'POST',
            data        :  $("#eu_order").serialize(),
            headers     : {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            dataType    : "json",
            beforeSend  : function () {
                $("#loader-section").css('display','block');
                //return false;
            },
            complete: function () {
                $("#loader-section").css('display','none');
            },
            success: function (response) {
               $.toast({
                    heading             : 'Success',
                    text                : response.success_message,
                    loader              : true,
                    loaderBg            : '#fff',
                    showHideTransition  : 'fade',
                    icon                : 'success',
                    hideAfter           : response.delayTime,
                    position            : 'top-right'
                });
               setTimeout(function() { location.reload();}, response.delayTime);
            },
            error:function(response){
                $.toast({
                    heading             : 'Error',
                    text                : response.errors,
                    loader              : true,
                    loaderBg            : '#fff',
                    showHideTransition  : 'fade',
                    icon                : 'error',
                    hideAfter           : delayTime,
                    position            : 'top-right'
                });
            }
        });
    });

    // Balance detail 
    $('body').on('click','.view-balance-details',function(event) {
        event.preventDefault();
        var id = $(this).attr("data-id");
        var getUrl = base_url+'/balance-detail';        
        $.ajax({
            url         : getUrl,
            type        : 'POST',
            data        : {id:id},
            headers     : {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            dataType    : "json",
            beforeSend  : function () {
                //$("#loader-section").css('display','block');
                //return false;
            },
            complete: function () {
                //$("#loader-section").css('display','none');
            },
            success: function (response) {
                jQuery("#detailResult").html(response.html);
                jQuery("#allResult").hide();
                jQuery("#detailResult").show();
            },
            error:function(response){
            }
        });
    });

     // Balance detail Customer
    $('body').on('click','.cus-view-balance-details',function(event) {
        event.preventDefault();
        var id      = $(this).attr("data-id");
        var email   = $(this).attr("data-email");
        var name    = $(this).attr("data-name");        
        var orderEndDate = $('#order_end_date').val();
        var orderStartDate = $('#order_start_date').val();

        var getUrl = base_url+'/cusbalance-detail';        
        $.ajax({
            url         : getUrl,
            type        : 'POST',
            data        : {id:id,name:name,email:email,start_date:orderStartDate,end_date:orderEndDate},
            headers     : {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            dataType    : "json",
            beforeSend  : function () {
                //$("#loader-section").css('display','block');
                //return false;
            },
            complete: function () {
                //$("#loader-section").css('display','none');
            },
            success: function (response) {
                jQuery("#detailResult").html(response.html);
                jQuery("#allResult").hide();
                jQuery("#detailResult").show();
            },
            error:function(response){
            }
        });
    });

    // Balance detail Supplier
    $('body').on('click','.sup-view-balance-details',function(event) {
        event.preventDefault();
        var id = $(this).attr("data-id");
        var orderEndDate = $('#order_end_date').val();
        var orderStartDate = $('#order_start_date').val();

        var getUrl = base_url+'/supbalance-detail';        
        $.ajax({
            url         : getUrl,
            type        : 'POST',
            data        : {id:id,start_date:orderStartDate,end_date:orderEndDate},
            headers     : {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            dataType    : "json",
            beforeSend  : function () {
                $("#loader-section").css('display','block');
                //return false;
            },
            complete: function () {
                $("#loader-section").css('display','none');
            },
            success: function (response) {
                jQuery("#detailResult").html(response.html);
                jQuery("#allResult").hide();
                jQuery("#detailResult").show();
            },
            error:function(response){
            }
        });
    });


    // Show Appeal list back
    $('body').on('click','.show-appeal-back',function(event) {       
        jQuery("#appealDetail").hide();
        jQuery("#appealOrderDetail").hide();
        jQuery("#allResult").show();
    });

    // Show Appeal Orders list back
    $('body').on('click','.show-appeal-order-back',function(event) {
        jQuery("#allResult").hide();        
        jQuery("#appealOrderDetail").hide();
        jQuery("#appealDetail").show();
    });

    // Show Orders back list back
    $('body').on('click','.show-order-back',function(event) {
        jQuery("#allResult").show();
        jQuery("#detailResult").hide();
    });

    // Custome price upload popup show
    $('body').on('click','.customer_price_upload',function(event) {
        var cusId = $(this).attr('data-id');
        $('#CustomerPrice_Popup').modal('toggle');
        $('#customer_id').val(cusId);      
    });    


     //Edit tracking number
    $('body').on('click','.edit_tracking_number',function(event) {
        var tn = $('#span_tn').text();
        var nop = $('#span_nop').text();
        var customer_shipping = $('#span_cus_shipping').text();
        var orderId = $(this).attr('data-orderId');
        $('#order_id_hidden').val(orderId);
        $('#no_of_parcel').val(nop);
        $('#customer_shipping').val(customer_shipping);
        $('#edit_tracking_number').text(tn);
        $("#edit_tracking_number-error").hide();
        $('#Edit_tracking_number').modal('toggle');      
    });


    $('body').on('click','.edit_tracking_number_btn', function(event){

        var orderId = $('#order_id_hidden').val();
        var nop = $('#no_of_parcel').val();
        var tn = $('#edit_tracking_number').val();       
        var customer_shipping = $('#customer_shipping').val();       
        var tnArr = tn.split(',');        
        if(parseInt(tnArr.length) != parseInt(nop)){
            $("#edit_tracking_number-error").html('Tracking number should be same as number of parcel.');
            $("#edit_tracking_number-error").show();
            return false;
        }

        $patrenError = 'no';
        $.each(tnArr , function(index, val) { 
            //console.log(index, val)
            var trackingPatren = new RegExp("^[A-Z]{2}[0-9]{9}[A-Z]{2}$");
            //val = val.replace(/ /g,'');
            val = $.trim(val);
            if (!trackingPatren.test(val)) {
                $patrenError = 'yes';
            } 
        });
    
        if($patrenError == 'yes'){
            $("#edit_tracking_number-error").html('Invalid tracking number.Format should be (AB123456789YZ)');
            $("#edit_tracking_number-error").show();
            return false;
        }           

        if(customer_shipping == ''){
            $("#customer_shipping-error").html('Please enter customer shipping cost.');
            $("#customer_shipping-error").show();
            return false;
        }  
        var currentThis = $(this);        
        $(this).prop("disabled", true);    
        var getUrl = base_url+'/edit-tracking-number';
        $.ajax({
            url         : getUrl,
            type        : 'GET',
            data        : {id:orderId,nop:nop,tn:tn,customer_shipping:customer_shipping},
            headers     : {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            dataType    : "json",
            beforeSend  : function () {
                $("#loader-section").css('display','block');              
            },
            complete: function () {
                 $("#loader-section").css('display','none');
            },
            success: function (response) {
                currentThis.prop("disabled", false);
                $('#span_tn').text(response.tracking_number);
                $('#span_nop').text(response.number_of_parcel);
                $('#span_cus_shipping').text(response.customer_shipping);
                $('#span_cus_shipping_total').text(response.customer_shipping_total);
                $('#Edit_tracking_number').modal('hide');
                $.toast({
                    heading             : 'Success',
                    text                : response.success_message,
                    loader              : true,
                    loaderBg            : '#fff',
                    showHideTransition  : 'fade',
                    icon                : 'success',
                    hideAfter           : response.delayTime,
                    position            : 'top-right'
                });
            },
            error:function(response){

                $.toast({
                    heading             : 'Error',
                    text                : response.errors,
                    loader              : true,
                    loaderBg            : '#fff',
                    showHideTransition  : 'fade',
                    icon                : 'error',
                    hideAfter           : delayTime,
                    position            : 'top-right'
                });
            }
        });
    });

    // Supplier price upload popup show
    $('body').on('click','.supplier_price_upload',function(event) {
        var supId = $(this).attr('data-id');
        $('#SupplierPrice_Popup').modal('toggle');
        $('#supplier_id').val(supId);      
    });  

    // Assign Supplier Get Selected row
    $('body').on('click','.assign-supplier-btn',function(event) {
        
        var checkedCount = $("#tableBody input[name='checkrow[]']:checked").length;
        
        var checkedVals = $("#tableBody input[name='checkrow[]']:checked").map(function() {
            return this.value;
        }).get();
        if(checkedVals.join(",") == ''){
            //$('.diaglog-modal-title').html('Message');
            $('#dialogModal .modal-body').html('<p> Please check aleast one row for assign supplier </p>');
            $('#dialogModal').modal('toggle');
        }else{
            $('#selectedRows').val(checkedVals.join(","));
            $('#appealId').val($('#appealid').val());
            //$('#deletedRows').val($('#deleterow').val());
            $('#rowsCount').val($('#rowscount').val());
            $('#Assign_Supplier').modal('toggle');
        }
    });

     // Assign Tracking Number Get Selected row
    $('body').on('click','.assign-tracking-btn',function(event) {
        var checkedVals = $("#tableBody input[name='checkrow[]']:checked").map(function() {
            return this.value;
        }).get();
        //alert(checkedVals.join(","));
        //return false;
        if(checkedVals.join(",") == ''){
            //$('.diaglog-modal-title').html('Message');
            $('#dialogModal .modal-body').html('<p> Please select aleast one order item to assign tracking number.</p>');
            $('#dialogModal').modal('toggle');
        }else{

            $('#selectedRows').val(checkedVals.join(","));
            $('#orderId').val($('#orderid').val());
            //$('#deletedRows').val($('#deleterow').val());
            $('#rowsCount').val($('#rowscount').val());
            $('#Assign_Tracking').modal('toggle');
        }
    });


    // Delete Request Batch
    $('body').on('click','.delete-req-batch',function(event) {
        var checkedVals = $('.checkrow:checkbox:checked').map(function() {
            return this.value;
        }).get();
        //alert(checkedVals.join(","));
        if(checkedVals.join(",") == ''){
            //$('.diaglog-modal-title').html('Message');
            $('#dialogModal .modal-body').html('<p> Please check aleast one request batch to delete. </p>');
            $('#dialogModal').modal('toggle');
        }else{
            $('.deleteReqBatch').attr('data-deletedRows',checkedVals.join(","));
            $('#deleteReqBatchModal').modal('toggle');
        }
    });


    jQuery(document).on('click', '.deleteReqBatch', function (event) {
        event.preventDefault();
        var deletedRows = $(this).attr('data-deletedRows');
        
        $.ajax({
            url         : base_url+'/delete-req-batch',
            type        : 'POST',
            data        : {deletedRows:deletedRows},
            headers     : {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            dataType    : "json",
            beforeSend  : function () {
                //$("#loader-section").css('display','block');
                //return false;
            },
            complete: function () {
                //$("#loader-section").css('display','none');
            },
            success: function (response) {    
                $('#deleteReqBatchModal').modal('hide');           
                $.toast({
                    heading             : 'Success',
                    text                : response.success_message,
                    loader              : true,
                    loaderBg            : '#fff',
                    showHideTransition  : 'fade',
                    icon                : 'success',
                    hideAfter           : response.delayTime,
                    position            : 'top-right'
                });                
                setTimeout(function() { location.reload();}, response.delayTime);
            },
            error:function(response){
                // console.log(response);
                $.toast({
                        heading             : 'Error',
                        text                : response.errors,
                        loader              : true,
                        loaderBg            : '#fff',
                        showHideTransition  : 'fade',
                        icon                : 'error',
                        hideAfter           : response.delayTime,
                        position            : 'top-right'
                    });
            }
        });
    });

     // Dipatch Regular Order Get Selected row
    $('body').on('click','.regular-dispatch-btn',function(event) {
        var checkedVals = $('.checkrow:checkbox:checked').map(function() {
            return this.value;
        }).get();
        //alert(checkedVals.join(","));
        //return false;
        if(checkedVals.join(",") == ''){
            //$('.diaglog-modal-title').html('Message');
            $('#dialogModal .modal-body').html('<p> Please select aleast one order item to dispatch.</p>');
            $('#dialogModal').modal('toggle');
        }else{
            $('#selectedRows').val(checkedVals.join(","));
            $('#orderId').val($('#orderid').val());
            //$('#deletedRows').val($('#deleterow').val());
            $('#rowsCount').val($('#rowscount').val());
            $('#Dispatch_regular_order').modal('toggle');
        }
    });

    $('body').on('click','.checkall', function(event){
        $('input:checkbox').not(this).prop('checked', this.checked);
    });

    jQuery(document).on('click','.place_order_btn',function(event) {

        if($(this).attr("data-isfilevalid") == 'yes'){
             $("form:first").submit();
        }else{
            //alert('Some products are out of stock or not applicable.');
             $('#dialogModal .modal-body').html('<p> Some products are out of stock or not applicable. </p>');
            $('#dialogModal').modal('toggle');            
        }

    });

    jQuery(document).on('click','.upload-excel-file',function(event) {
        var file = jQuery('#orderexcelFile').val();
        var extension = file.substr( (file.lastIndexOf('.') +1) );
        if (file == ''){
            jQuery('.fileerror').html('Please upload order excel file');
        }else if(extension == 'xls' || extension == 'xlsx' || extension == 'csv'){
            $("form:first").submit();
        }else{
            jQuery('.fileerror').html('Please upload only excel file');
        }
    });

    jQuery(document).on('click','.upload_product_file',function(event) {
        var file = jQuery('#productsFile').val();
        var extension = file.substr( (file.lastIndexOf('.') +1) );
        if (file == ''){
            jQuery('.fileerror').html('Please upload order excel file');
        }else if(extension == 'xls' || extension == 'xlsx' || extension == 'csv'){
            $("form:first").submit();
        }else{
            jQuery('.fileerror').html('Please upload only excel file');
        }
    });

    jQuery(document).on('click','.tracking-excel-file',function(event) {
        var file = jQuery('#update_trackking_number').val();
        var extension = file.substr( (file.lastIndexOf('.') +1) );
        if (file == ''){
            jQuery('.fileerror').html('Please upload order excel file');
        }else if(extension == 'xls' || extension == 'xlsx' || extension == 'csv'){
            $("form:first").submit();
        }else{
            jQuery('.fileerror').html('Please upload only excel file');
        }
    });

    jQuery(document).on('click','.upload-supplier-price-file',function(event) {
        var file = jQuery('#supplier_price_file').val();
        var extension = file.substr( (file.lastIndexOf('.') +1) );
        if (file == ''){
            jQuery('.fileerror').html('Please upload price excel file');
        }else if(extension == 'xls' || extension == 'xlsx' || extension == 'csv'){
            $("form:first").submit();
        }else{
            jQuery('.fileerror').html('Please upload only excel file');
        }
    });

    jQuery(document).on('click','.upload-custome-price-file',function(event) {
        var file = jQuery('#customer_price_file').val();
        var extension = file.substr( (file.lastIndexOf('.') +1) );
        if (file == ''){
            jQuery('.fileerror').html('Please upload price excel file');
        }else if(extension == 'xls' || extension == 'xlsx' || extension == 'csv'){
            $("form:first").submit();
        }else{
            jQuery('.fileerror').html('Please upload only excel file');
        }
    });

    $('body').on('click','.assignTracking', function(event){
        jQuery('.assignTracking').attr("disabled", "disabled");
        var selectedRows = jQuery('#selectedRows').val();
        var orderId = jQuery('#orderId').val();
        var rowsCount = jQuery('#rowsCount').val();
        
        var order_type = jQuery('#ordertype').val();
        if(order_type == 'Register'){
            var tracking_number = jQuery('#tracking_number').val();
        }else{
            var tracking_number = 'Okay';
        }
        var getUrl = base_url+'/assign-tracking';

        $.ajax({
            url         : getUrl,
            type        : 'POST',
            data        : {orderId:orderId,selectedRows:selectedRows,tracking_number:tracking_number,rowsCount:rowsCount,order_type:order_type},
            headers     : {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            dataType    : "json",
            beforeSend  : function () {
                //$("#loader-section").css('display','block');
                //return false;
            },
            complete: function () {
                //$("#loader-section").css('display','none');
            },
            success: function (response) {
                if (response.success)
                {
                    $('#Assign_Tracking').modal('hide');
                    $.toast({
                        heading             : 'Success',
                        text                : response.success_message,
                        loader              : true,
                        loaderBg            : '#fff',
                        showHideTransition  : 'fade',
                        icon                : 'success',
                        hideAfter           : response.delayTime,
                        position            : 'top-right'
                    });
                    if(response.url && response.url != '')
                    {
                        if(response.delayTime)
                            setTimeout(function() { window.location.href=response.url;}, response.delayTime);
                        else
                            window.location.href=response.url;
                    }
                }
                else
                {  
                    setTimeout(function() {
                        $.toast({
                            heading             : 'Error',
                            text                : response.errors,
                            loader              : true,
                            loaderBg            : '#fff',
                            showHideTransition  : 'fade',
                            icon                : 'error',
                            hideAfter           : response.delayTime,
                            position            : 'top-right'
                        });
                        jQuery('.assignTracking').removeAttr("disabled");
                    }, response.delayTime);
                }
            },
            error:function(response){
                // console.log(response);
                // console.log('sdsdsds');
            }
        });

    });

    $('body').on('click','.cancel-button', function(event){
        var url = $(this).attr("data-url");
        window.location.href = url;
    });

    $('body').on('click','.download-order-excel', function(event){
        event.preventDefault();
        var orderId = $(this).attr("data-orderid");
        var getUrl = base_url+'/order-download';

        $.ajax({
            url         : getUrl,
            type        : 'GET',
            data        : {id:orderId},
            headers     : {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            dataType    : "json",
            beforeSend  : function () {
                //$("#loader-section").css('display','block');
                //return false;
            },
            complete: function () {
                //$("#loader-section").css('display','none');
            },
            success: function (response) {
               jQuery("#allResult").html(response.html);
            },
            error:function(response){
                // console.log(response);
                // console.log('sdsdsds');
            }
        });
    });

    $('body').on('click','.appeal-delete-key', function(event){
        event.preventDefault();
        var rowKey = $(this).attr('data-id');
        var deleteRowValue = $('#deleterow').val();
        if(deleteRowValue == ''){
            deleteRowValue = rowKey;
        }else{
            deleteRowValue = deleteRowValue+','+rowKey;
        }
        $(this).closest('tr').remove();
        $('#deleterow').val(deleteRowValue);
        $('.allRows').each(function (index, value) {
           $(this).children('td').first().text(index + 1);
         });
    });

    $('body').on('click','.rejectOrder', function(event){
        var row_index = $(this).parents('tr').index(); 
        var orderId = $(this).attr('data-orderid');        
        $('.rejectOrderConfirm').attr('data-orderid',orderId);
        $('.rejectOrderConfirm').attr('data-rowindex',row_index);
    });

    $('body').on('click','.deleteCustomer', function(event){
        //var row_index = $(this).parents('tr').index(); 
        var row_index = $(this).parents('tr').attr('class'); 
        var cusid = $(this).attr('data-cusid');       
        $('.deleteCustomerConfirm').attr('data-cusid',cusid);
        $('.deleteCustomerConfirm').attr('data-rowindex',row_index);
    });

    $('body').on('click','.deleteSupplier', function(event){
        //var row_index = $(this).parents('tr').index(); 
        var row_index = $(this).parents('tr').attr('class'); 
        var cusid = $(this).attr('data-cusid');       
        $('.deleteSupplierConfirm').attr('data-cusid',cusid);
        $('.deleteSupplierConfirm').attr('data-rowindex',row_index);
    });


    $('body').on('click','.reshipOrder', function(event){
        var row_index = $(this).parents('tr').index(); 
        var orderId = $(this).attr('data-orderid');        
        $('.reshipOrderConfirm').attr('data-orderid',orderId);
        $('.reshipOrderConfirm').attr('data-rowindex',row_index);
    });

    $('body').on('click','.redirectOrder', function(event){         
        var orderId = $(this).attr('data-orderid');        
        //$('#redirectOrder').val(orderId);
        $.ajax({
            url         : base_url+'/get-redirect-model',
            type        : 'POST',
            data        : {id:orderId},
            headers     : {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            dataType    : "html",
            beforeSend  : function () {
                //$("#loader-section").css('display','block');
                //return false;
            },
            complete: function () {
                //$("#loader-section").css('display','none');
            },
            success: function (response) {
                $(".redirect-order-modal-body").html(response);
            },
            error:function(response){
            }   
        });
    });
  
    //$('body').on('click','.rejectOrderConfirm', function(event){
    jQuery(document).on('click', '.rejectOrderConfirm', function (event) {
        event.preventDefault();
        var orderId = $(this).attr('data-orderid');
        var rowIndex = $(this).attr('data-rowindex');
        var parentTr = $(this).closest('tr');
        
        $.ajax({
            url         : base_url+'/reject-order',
            type        : 'POST',
            data        : {id:orderId,rowIndex:rowIndex},
            headers     : {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            dataType    : "json",
            beforeSend  : function () {
                //$("#loader-section").css('display','block');
                //return false;
            },
            complete: function () {
                //$("#loader-section").css('display','none');
            },
            success: function (response) {
                //console.log(parentTr);  
                $('.row'+response.row_index).find('td.actionTd').find('.rejectOrder').remove();
                $('.row'+response.row_index).find('td.statusTd').html('Rejected');
                $('.row'+response.row_index).find('td.paymentTd').find('.orderAcceptPayment').attr('disabled','');
                $('.row'+response.row_index).find('td.paymentTd').find('.lable'+response.row_index).attr('title','Order Rejected');
                $('#rejectOrderModal').modal('hide');

                $.toast({
                    heading             : 'Success',
                    text                : response.success_message,
                    loader              : true,
                    loaderBg            : '#fff',
                    showHideTransition  : 'fade',
                    icon                : 'success',
                    hideAfter           : response.delayTime,
                    position            : 'top-right'
                });
                
                //setTimeout(function() { location.reload();}, response.delayTime);
                setTimeout(function() {
                    $( ".filterResult" ).trigger( "click" );
                },1);

            },
            error:function(response){
                // console.log(response);
                $.toast({
                        heading             : 'Error',
                        text                : response.errors,
                        loader              : true,
                        loaderBg            : '#fff',
                        showHideTransition  : 'fade',
                        icon                : 'error',
                        hideAfter           : response.delayTime,
                        position            : 'top-right'
                    });
            }
        });
    });

    jQuery(document).on('click', '.deleteCustomerConfirm', function (event) {
        event.preventDefault();
        var cusId = $(this).attr('data-cusid');
        var rowIndex = $(this).attr('data-rowindex');
        var parentTr = $('.'+rowIndex);
        $.ajax({
            url         : base_url+'/delete-customer',
            type        : 'POST',
            data        : {id:cusId,rowIndex:rowIndex},
            headers     : {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            dataType    : "json",
            beforeSend  : function () {
                //$("#loader-section").css('display','block');
                //return false;
            },
            complete: function () {
                //$("#loader-section").css('display','none');
            },
            success: function (response) {
                $('#deleteCustomerModal').modal('hide');
                parentTr.remove();
                setTimeout(function() { location.reload();}, response.delayTime);  
                $.toast({
                    heading             : 'Success',
                    text                : response.success_message,
                    loader              : true,
                    loaderBg            : '#fff',
                    showHideTransition  : 'fade',
                    icon                : 'success',
                    hideAfter           : response.delayTime,
                    position            : 'top-right'
                }); 
                
            },
            error:function(response){
                // console.log(response);
                $.toast({
                        heading             : 'Error',
                        text                : response.errors,
                        loader              : true,
                        loaderBg            : '#fff',
                        showHideTransition  : 'fade',
                        icon                : 'error',
                        hideAfter           : response.delayTime,
                        position            : 'top-right'
                    });
            }
        });
    });

    jQuery(document).on('click', '.deleteSupplierConfirm', function (event) {
        event.preventDefault();
        var cusId = $(this).attr('data-cusid');
        var rowIndex = $(this).attr('data-rowindex');
        var parentTr = $('.'+rowIndex);
        var rowCount = parentTr.parents('table').find('tr').length;
      
        $.ajax({
            url         : base_url+'/delete-supplier',
            type        : 'POST',
            data        : {id:cusId,rowIndex:rowIndex},
            headers     : {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            dataType    : "json",
            beforeSend  : function () {
                //$("#loader-section").css('display','block');
                //return false;
            },
            complete: function () {
                //$("#loader-section").css('display','none');
            },
            success: function (response) {                
                $('#deleteSupplierModal').modal('hide');
                parentTr.remove();
                setTimeout(function() { location.reload();}, 100);
                $.toast({
                    heading             : 'Success',
                    text                : response.success_message,
                    loader              : true,
                    loaderBg            : '#fff',
                    showHideTransition  : 'fade',
                    icon                : 'success',
                    hideAfter           : response.delayTime,
                    position            : 'top-right'
                });                   
            },
            error:function(response){
                // console.log(response);
                $.toast({
                        heading             : 'Error',
                        text                : response.errors,
                        loader              : true,
                        loaderBg            : '#fff',
                        showHideTransition  : 'fade',
                        icon                : 'error',
                        hideAfter           : response.delayTime,
                        position            : 'top-right'
                    });
            }
        });
    });


     jQuery(document).on('click', '.reshipOrderConfirm', function (event) {
        event.preventDefault();
        var orderId = $(this).attr('data-orderid');
        var rowIndex = $(this).attr('data-rowindex');
        var parentTr = $(this).closest('tr');
        
        $.ajax({
            url         : base_url+'/reship-order',
            type        : 'POST',
            data        : {id:orderId,rowIndex:rowIndex},
            headers     : {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            dataType    : "json",
            beforeSend  : function () {
                //$("#loader-section").css('display','block');
                //return false;
            },
            complete: function () {
                //$("#loader-section").css('display','none');
            },
            success: function (response) {
                //console.log(parentTr);  
                $('.row'+response.row_index).find('td.actionTd').find('.rejectOrder').remove();
                $('.row'+response.row_index).find('td.statusTd').html('Rejected');
                $('.row'+response.row_index).find('td.paymentTd').find('.orderAcceptPayment').attr('disabled','');
                $('.row'+response.row_index).find('td.paymentTd').find('.lable'+response.row_index).attr('title','Order Rejected');
                $('#rejectOrderModal').modal('hide');

                $.toast({
                    heading             : 'Success',
                    text                : response.success_message,
                    loader              : true,
                    loaderBg            : '#fff',
                    showHideTransition  : 'fade',
                    icon                : 'success',
                    hideAfter           : response.delayTime,
                    position            : 'top-right'
                });
                
                setTimeout(function() { location.reload();}, response.delayTime);

            },
            error:function(response){
                // console.log(response);
                $.toast({
                        heading             : 'Error',
                        text                : response.errors,
                        loader              : true,
                        loaderBg            : '#fff',
                        showHideTransition  : 'fade',
                        icon                : 'error',
                        hideAfter           : response.delayTime,
                        position            : 'top-right'
                    });
            }
        });
    });

    $('body').on('click','.deleteAppealId', function(event){
      var rowId = $(this).attr('data-orderid');
      var orderId = $(this).attr('data-id');
      $('.deleteAdminOrder').attr('data-id',orderId);
      $('.deleteAdminOrder').attr('data-orderid',rowId);
    });

    $('body').on('click','.deleteAdminOrder', function(event){
        event.preventDefault();
        var orderId = $(this).attr('data-orderid');
        var rowId = $(this).attr('data-id');
        if(rowId != ''){
          __item_ref = rowId+',';
             $('.deleterow').val(__item_ref);
        }

        $.ajax({
            url         : base_url+'/delete-order-admin',
            type        : 'POST',
            data        : {deleted_rows:__item_ref,id:orderId},
            headers     : {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            dataType    : "json",
            beforeSend  : function () {
                //$("#loader-section").css('display','block');
                //return false;
            },
            complete: function () {
                //$("#loader-section").css('display','none');
            },
            success: function (response) {
              var splitIds = __item_ref.split(",");
              $.each(splitIds,function(i){
                 // $('#row-'+splitIds[i]).addClass('unselectable');
                 $('#row-'+splitIds[i]).find('.status').text('Deleted');
                 $('#row-'+splitIds[i]).find('.checkrow').hide();
                 $('#row-'+splitIds[i]).find('.deleteAppealId').hide();
              });
              $('button[data-dismiss="modal"]').trigger("click");
            },
            error:function(response){
                // console.log(response);
            }
        });
    });

    $('body').on('click','.deleteProductConfirm', function(event){
        event.preventDefault();
   
        $.ajax({
            url         : base_url+'/delete-all-products',
            type        : 'POST',
            //data        : {},
            headers     : {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            dataType    : "json",
            beforeSend  : function () {
                //$("#loader-section").css('display','block');
                //return false;
            },
            complete: function () {
                //$("#loader-section").css('display','none');
            },
            success: function (response) {
                $.toast({
                    heading             : 'Success',
                    text                : response.success_message,
                    loader              : true,
                    loaderBg            : '#fff',
                    showHideTransition  : 'fade',
                    icon                : 'success',
                    hideAfter           : response.delayTime,
                    position            : 'top-right'
                });                
                setTimeout(function() { location.reload();}, response.delayTime);    
            },
            error:function(response){
                 $.toast({
                    heading             : 'Error',
                    text                : response.errors,
                    loader              : true,
                    loaderBg            : '#fff',
                    showHideTransition  : 'fade',
                    icon                : 'error',
                    hideAfter           : response.delayTime,
                    position            : 'top-right'
                });
            }
        });
    });

    jQuery(document).on('click', '.editTracking', function () {
      var url = jQuery(this).attr('data-url');
      var id = $(this).attr('data-orderid');
      $(this).closest('tr').find('.editIcon').hide();
      var appendText = $('<input type="text" class="tracking" value=""><a class="saveTracking" data-url="'+url+'" data-orderid="'+id+'"><i class="fa fa-check tickIcon"></i></a>');
      $(this).closest('tr').find('.editText').html(appendText);
    });

    jQuery(document).on('click', '.saveTracking', function () {
      var ele = jQuery(this);
      var url = jQuery(this).attr('data-url');
      var order_id = $(this).attr('data-orderid');
      var parentTd = $(this).closest('td');
      var parentTr = $(this).closest('tr');
      var track_num = $(this).closest('tr').find('.tracking').val();
      $.ajax({
        type:     'post',
        data:    {tracking_number:track_num,id:order_id},
        url:     url+'/order-tracking-id',
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success:function(response){
          console.log(response);
          if(response.success)
          {
             $('.saveTracking').closest('tr').find('.tickIcon').hide();
             var trackingNo = $('<span class="savingNumber">'+track_num+'</span>');
             parentTd.html(trackingNo);
             //parentTr.find('.status_td').html('');
             //parentTr.find('.order_status').val("2").change();
             parentTr.find('.order_status').val("2");
             $('.tracking').hide();
             $.toast({
                 heading             : 'Success',
                 text                : response.success_message,
                 loader              : true,
                 loaderBg            : '#fff',
                 showHideTransition  : 'fade',
                 icon                : 'success',
                 position            : 'top-right'
             });
          }
          else
           {
            $.toast({
                heading             : 'Error',
                text                : response.errors,
                loader              : true,
                loaderBg            : '#fff',
                showHideTransition  : 'fade',
                icon                : 'error',
                position            : 'top-right'
            });
          }
         },
        error:function(response){
        }
      });

    });

    // Order change status by supplier
    var previousStatus = '';
    jQuery(document).on('change', '.order_status', function () {
        previousStatus = jQuery(this).val();
    });

    jQuery(document).on('change', '.order_status', function () {
        var status = jQuery(this).val();
        var url = jQuery(this).attr('data-url');
        var order_id = $(this).attr('data-orderid');
        var parentTr = $(this).closest('tr');

        $.ajax({
            type:    'post',
            data:    {status:status,id:order_id},
            url:     url+'/order-change-status',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success:function(response){
                //console.log(response);
                if(response.success)
                {
                    parentTr.find('.order_status ').find("option[value='"+previousStatus+"']").prevAll().prop("disabled", true);
                    $.toast({
                        heading             : 'Success',
                        text                : response.success_message,
                        loader              : true,
                        loaderBg            : '#fff',
                        showHideTransition  : 'fade',
                        icon                : 'success',
                        position            : 'top-right'
                    });
                }else{
                    $.toast({
                        heading             : 'Error',
                        text                : response.errors,
                        loader              : true,
                        loaderBg            : '#fff',
                        showHideTransition  : 'fade',
                        icon                : 'error',
                        position            : 'top-right'
                    });
                }
            },
            error:function(response){
    $(".redirect_price").each(function()
    {
        $(this).rules('remove');
        $(this).rules('add', {
            required: true,
            messages: {
                required: "Please enter Price."
            },
        });
    });
            }
        });
    });

    
    jQuery(document).on('click', '.customerFormBtn', function () {
        if(customerCustomValidation()){            
        }
    });

    jQuery(document).on('click', '.remove_sg_shipp_rate', function () {        
        $rowCount = $(this).attr('data-count');        
        //$(this).closest('.shipping_rates').prev().remove();  
        var lastRow = $(this).closest('.shipping_rates').prev();  
        $(this).closest('.shipping_rates').remove();  
        lastRow.find(".addremovediv").css("display", "block");
        lastRow.find('.remove_sg_shipp_rate').show();   
        lastRow.find('.add_new_sg_shipp_rate').show();   
    });

    jQuery(document).on('click', '.add_new_sg_shipp_rate', function () {
        // Run New added field validation      
        customerCustomValidation();
        if($("#customer-form").valid()){            
            var nextIndex = $(this).attr('data-count');                    
            //$(this).closest('.shipping_rates').find('.topills').next().remove();
            //$(this).closest('.shipping_rates').find('.sg_shipping_rate').next().remove();
            //$(this).closest('.shipping_rates').find('.sg_shipping_rate').next('lavel:contains("Please enter shipping rate")').remove();
            //return false;
            /* if($(this).closest('.shipping_rates').find('.topills').val() == ''){ 
                $(this).closest('.shipping_rates').find('.topills').parent('div').append('<label class="error">Please enter to pills.</label>');                      
                return false;
            }else{
                if($(this).closest('.shipping_rates').find('.sg_shipping_rate').val() == ''){ 
                    $(this).closest('.shipping_rates').find('.sg_shipping_rate').parent('div').append('<label class="error">Please enter shipping rate.</label>');                      
                    return false;
                }       
                if(parseInt($(this).closest('.shipping_rates').find('.topills').val()) <= parseInt($(this).closest('.shipping_rates').find('.frompills').val())){
                    $(this).closest('.shipping_rates').find('.topills').parent('div').append('<label class="error">greater then from pills.</label>');
                    return false;
                }
            }*/     
            var newRow = $(this).closest('.shipping_rates').clone(); 
            $(this).closest('.shipping_rates').find('.remove_sg_shipp_rate').hide();   
            $(this).closest('.shipping_rates').find('.add_new_sg_shipp_rate').hide();   
            newRow.find('.frompills').val($(this).closest('.shipping_rates').find('.topills').val());
            newRow.find(".shipp_rate_id").val("");
            newRow.find(".topills").val("");
            newRow.find(".sg_shipping_rate").val("");
            newRow.find(".topills").removeAttr("readonly");
            newRow.find("button").addClass("clonebtn");
            newRow.find(".sg_shipping_rate").attr('name', 'sg_shipping_rate['+nextIndex+']');
            newRow.find(".topills").attr('name', 'topills['+nextIndex+']');
            newRow.find(".sg_shipping_rate").next().remove();
            newRow.find(".topills").next().remove();
            newRow.find(".remove_sg_shipp_rate").attr("data-count", Number(nextIndex)+1);
            newRow.find(".add_new_sg_shipp_rate").attr("data-count", Number(nextIndex)+1);
            //$(this).remove();  
            $("#sg_shipping_rates").append(newRow);      
        }         
    });

    function customerCustomValidation(){
        $(".topills").each(function()
        {
            $(this).rules('remove');
            $(this).rules('add', {
                required: true,
                messages: {
                    required: "Please enter to pills."
                },
            });
        });
        $(".sg_shipping_rate").each(function()
        {
            //$(this).rules('remove');
            $(this).rules('add', {
                required: true,
                messages: {
                    required: "Please enter shipping rate."
                },
            });
        });    
    }


     $(".topills").each(function()
        {
            $(this).rules('remove');
            $(this).rules('add', {
                required: true,
                messages: {
                    required: "Please enter to pills."
                },
            });
        });
        $(".sg_shipping_rate").each(function()
        {
            //$(this).rules('remove');
            $(this).rules('add', {
                required: true,
                messages: {
                    required: "Please enter shipping rate."
                },
            });
        });

    jQuery(document).on('click', '.remove_new_sg_shipp_rate', function () {
        $(this).closest('.shipping_rates').remove();
    });
    jQuery(document).on('keyup', '.topills', function () {
        $(this).parent('div').find('.errorlable').remove();
        if(parseInt($(this).val()) <= parseInt($(this).closest('.shipping_rates').find('.frompills').val())){
            $(this).parent('div').append('<label class="errorlable error">greater then from pills.</label>');
            return false;
        }
    });
    $('body').on('click','.alert_msg_dismiss',function(event) {
        $('#alert_msg_div').hide();      
        $('#alert_msg_p').html('');      
    });    
});
