@extends('layouts.app')
@section('content')
<div id="wrapper">
    <div id="allResult">               
        <div class="filterAdmin" id="allResult">               
            <div class="row text-center">
                <div class="col-md-10 latest_order_list">
                    <h3>User listing</h3>
                    <div class="table-responsive">
                        <table class="table table-bordered text-center">
                            <thead class="thead-dark">
                                <tr>
                                    <th>Sr No.</th>                                    
                                    <th>Client Name</th>                                
                                    <th>Client Email</th>                                   
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>                                
                               @if(count($usersDetails) > 0)                                
                                    @foreach ($usersDetails as $key => $user)
                                        <?php 
                                            $fetchAccounts = URL('/').'/fetch-accounts/'.$user->id;
                                            $UserInfo = Helper::getQuickbooksDetails($user->id);
                                            if(!empty($UserInfo))
                                            {
                                                $user_id  = $UserInfo[0]->user_id;                 
                                                if ($user->id == $user_id)
                                                {
                                                    $conn       = "yes";
                                                }
                                                else
                                                {
                                                    $conn       = "no";
                                                }   
                                            }
                                            else
                                            {
                                                $conn           = "no";
                                            }        
                                        ?>                                       
                                        <tr>
                                            <td>{{ $user->id}}</td>                                           
                                            <td>{{ $user->user_name }}</td>
                                            <td>{{ $user->user_email }}</td>
                                            <td>
                                               <div class="clsCont" style="text-align: center;">  
                                                    <?php 
                                                    if($conn == 'yes')
                                                    {                                                        
                                                        ?>
                                                        <a href="javascript:void(0)" title="Disconnect Quickbooks" class="diconnectUser" data-id="{{ $user->id }}"><img height="25" width="25" src="{{asset('assets/images/discon.png')}}"/>
                                                        <a>
                                                        <a href="{{ $fetchAccounts }}">View</a>
                                                    <?php 
                                                    }
                                                    else
                                                    { ?>
                                                        <a href="javascript:void(0)" title="Connect Quickbooks" data-ref="{{ $qb_url }}" class="getUserID" data-id="{{ $user->id }}"> <img height="25" width="25" src="{{asset('assets/images/qbimage.png')}}"/><a>
                                                    <?php 
                                                    }
                                                    ?>     
                                                </div> 
                                            </td>                                            
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="8"> No record found. </td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>                        
                    </div>
                </div>
            </div>
        </div>
    </div>    
</div>
@endsection
