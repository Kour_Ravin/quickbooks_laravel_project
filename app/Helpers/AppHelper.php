<?php
namespace App\Helpers;
use Session;
use DB;
use Helper;
use App\Http\Model\Common as Common; 
class AppHelper 
{ 
  
  // call Sageone Api  
  
  
  public static function getData($endpoint, $header) {
    $curl = curl_init($endpoint);
    curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "GET");
    curl_setopt($curl, CURLOPT_HEADER, false);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_HTTPHEADER, $header);    
    $response = curl_exec($curl);
    if (!$response) { /* Handle error */
    }    
    return $response;
  }
  
  
  public static function postData($endpoint, $post_data, $header) {    
    $curl = curl_init($endpoint);
    curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($curl, CURLOPT_HEADER, 0);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $post_data);
    curl_setopt($curl, CURLOPT_HTTPHEADER, $header);    
    if (curl_error($curl)) {
      $response = curl_error($curl);     
    }else
    {
      $response = curl_exec($curl); 
    }    
    return $response;
  }
  
  public static function qbConfig($accessToken,$refreshToken,$realm_id){    
    $detail = Common::getAppDetails();   
    $client_id = $detail[0]->client_id;
    $client_secret = $detail[0]->client_secret;  
    $auth_mode = $detail[0]->auth_mode;  
    $base_url = $detail[0]->base_url;  
    $service = array(
      'auth_mode' => $auth_mode,
      'ClientID' => $client_id,
      'ClientSecret' => $client_secret,
      'accessTokenKey' => $accessToken,
      'refreshTokenKey' => $refreshToken,
      'QBORealmID' => $realm_id,
      'baseUrl' => $base_url
    );
    return $service;
  }
  
  public static function getInvoiceLine($id) {   
    $table= 'invoicelineitem';
    $data = array(
      "invoiceId" => $id
    );
    $detail = Common::getAllRowData($table,$data);
    if(!empty($detail))
    {
      return $detail;
    }
  }

  public static function getQuickbooksDetails($userId)
  {
    $resultVal =  DB::table('quickbooks_details')->select( '*' )->where('user_id',$userId)->get()->toArray();
    return $resultVal;
  } 

  public static function checkDepartmentsExistsInDatabase($userId)
  {
    $resultVal =  DB::table('quickbooks_department')->select( '*' )->where('user_id',$userId)->get()->toArray();
    return $resultVal;
  }

  public static function getUsersQuickbooksDetails($userId = null)
  {
    if($userId != "")
    {
      $users = DB::table('users')
      ->Join('quickbooks_details', 'users.id', '=', 'quickbooks_details.user_id')
      ->where('user_id', $userId)
      ->get()
      ->toArray();
    }
    else
    {
      $users = DB::table('users')
      ->Join('quickbooks_details', 'users.id', '=', 'quickbooks_details.user_id')
      ->get()
      ->toArray();
    }
    return $users;
  }

  public static function qbConfigAndUserTokens($userId)
  {  
    $service       = array()  ;
    $detail        = Common::getAppDetails();   
    $client_id     = $detail[0]->client_id;
    $client_secret = $detail[0]->client_secret;  
    $auth_mode     = $detail[0]->auth_mode;  
    $base_url      = $detail[0]->base_url;  
    $userQbDetails = Helper::getQuickbooksDetails($userId);
    if(!empty($userQbDetails))
    {
      $user_id      = $userQbDetails[0]->user_id; 
      $accessToken  = $userQbDetails[0]->access_token; 
      $refreshToken = $userQbDetails[0]->refresh_token; 
      $realm_id     = $userQbDetails[0]->realm_id; 
      $service = array(
          'auth_mode' => $auth_mode,
          'ClientID' => $client_id,
          'ClientSecret' => $client_secret,
          'accessTokenKey' => $accessToken,
          'refreshTokenKey' => $refreshToken,
          'QBORealmID' => $realm_id,
          'baseUrl' => $base_url
        );
    }    
    return $service;
  }

  public static function echoprintcommand($data)
  {
    echo "<pre>";
    print_r($data);
    echo "</pre>";
  }
} 
?>
