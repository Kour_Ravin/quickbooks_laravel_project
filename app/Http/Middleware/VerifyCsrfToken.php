<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;

class VerifyCsrfToken extends BaseVerifier
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = ['set-userid-session','qb-disconnect','qb-user-disconnect','refresh-token','fetch-accounts','create-qb-journal'
        //
    ];
}
