<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use App\Http\Controllers\Controller;
use Auth;
use DateTime;
use Helper;
use DB;
use Validator;
use Hash;
use Session; 
use URL;
use Redirect;
use App\Http\Requests;
use App\Http\Model\Common as Common;
use App\Http\Model\QuickbooksDepartment as QuickbooksDepartment;
require_once('./vendor/autoload.php');
use QuickBooksOnline\API\Core\ServiceContext;
use QuickBooksOnline\API\DataService\DataService;
use QuickBooksOnline\API\PlatformService\PlatformService;
use QuickBooksOnline\API\Core\Http\Serialization\XmlObjectSerializer;
use QuickBooksOnline\API\Facades\Customer;
use QuickBooksOnline\API\Facades\Invoice;
use QuickBooksOnline\API\Facades\Item;
use QuickBooksOnline\API\Facades\Department;
use QuickBooksOnline\API\Facades\JournalEntry;
use QuickBooksOnline\API\Facades\Line;
use QuickBooksOnline\API\Facades\Account;


class QuickbooksController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
      set_time_limit(0);
      ini_set('display_errors', 1);
      ini_set('display_startup_errors', 1);
      error_reporting(E_ALL);
        $this->middleware('guest');
        $this->url =  URL('/');  
        $this->prefix='dashboard';  
    }

    //Quickbooks Configuration
    public function configData()
    {
        $detail        = Common::getAppDetails();   
        $client_id     = $detail[0]->client_id;
        $client_secret = $detail[0]->client_secret;  
        $base_url      = $detail[0]->base_url;  
        $authorizationRequestUrl = $detail[0]->authorizationRequestUrl;  
        $auth_mode     = $detail[0]->auth_mode;  
        $oauth_scope   = $detail[0]->oauth_scope;  
        $return        = array( 
                'authorizationRequestUrl' => $authorizationRequestUrl,
                'tokenEndPointUrl'        => 'https://oauth.platform.intuit.com/oauth2/v1/tokens/bearer',
                'auth_mode'               => $auth_mode,  
                'client_id'               => $client_id,
                'client_secret'           => $client_secret,
                'oauth_redirect_uri'      => $this->url.'/quickbooks-callback',
                'oauth_scope'             => $oauth_scope,
                'base_url'                => $base_url
        );
        return $return;
    }

     //Quickbooks Integration to get Auth Url
    public function getAuthorizationURL()
    {
        $config          = $this->configData();
        $dataService     = DataService::Configure(array(
          'auth_mode'    => $config['auth_mode'],
          'ClientID'     => $config['client_id'],
          'ClientSecret' => $config['client_secret'],
          'RedirectURI'  => $config['oauth_redirect_uri'],
          'scope'        => $config['oauth_scope'],
          'baseUrl'      => $config['base_url'] 
        ));
        $OAuth2LoginHelper = $dataService->getOAuth2LoginHelper();
        // Get the Authorization URL from the SDK
        $authUrl = $OAuth2LoginHelper->getAuthorizationCodeURL();
        return $authUrl;
    }

    //Set user id in session for Callback Function
    public function setUserIdSession(Request $request)
    {
      Session::put('userSessionId', $request->input('userid') );   
      if($request->session()->has('userSessionId'))
      {
        $output['success']          = true;
        $output['success_message']  = 'yes';
      }
      else
      {
        $output['success']          = false;
        $output['success_message']  = 'no';       
      }
      echo json_encode($output);
    }

    //This function is used when connection is established with Quickbooks to store Company id, Access Token and Refresh Token in database
    public function QuickbooksCallBack(Request $request)
    {
      $output   =   array();
      try 
      {
          $config         =   $this->configData();
          $dataService    =   DataService::Configure(array(
            'auth_mode'   =>  'oauth2',
            'ClientID'    =>  $config['client_id'],
            'ClientSecret'=>  $config['client_secret'],
            'RedirectURI' =>  $config['oauth_redirect_uri'],
            'scope'       =>  $config['oauth_scope'],
            'baseUrl'     =>  $config['base_url']         
          ));

          $OAuth2LoginHelper = $dataService->getOAuth2LoginHelper();
          $parseUrl          = $this->parseAuthRedirectUrl($_SERVER['QUERY_STRING']);

          /*
           * Update the OAuth2Token
           */
          $accessToken = $OAuth2LoginHelper->exchangeAuthorizationCodeForToken($parseUrl['code'], $parseUrl['realmId']);
          $dataService->updateOAuth2Token($accessToken);

          /*
           * Setting the accessToken for session variable
           */    

          Session::put('qb_access_token',$accessToken->getAccessToken());
          Session::put('refresh_token',$accessToken->getRefreshToken());
          Session::put('realmID',$accessToken->getRealmID());
          $value = array(
              'user_id' => Session::get('userSessionId'),
              'realm_id' => Session::get('realmID'),
              'access_token'=> Session::get('qb_access_token'),
              'refresh_token' => Session::get('refresh_token'),
              'created_at'=>date('Y-m-d H:i:s')
          ); 

          if(isset($value) && !empty($value))
          {
            Common::qbUpdateToken($value);
            Session::put('qblogin', "yes"); 
            $returnService = Helper::qbConfig(Session::get('qb_access_token'),Session::get('refresh_token'),Session::get('realmID'));
              $dataService = DataService::Configure($returnService);
              //echo '<pre>';print_r($dataService);die;
              $CompanyInfo = $dataService->getCompanyInfo();
              $error = $dataService->getLastError();
              if ($error) 
              {
                  $errorMsg = "The Status code is: " . $error->getHttpStatusCode() . "\n";
                  $errorMsg.= "The Helper message is: " . $error->getOAuthHelperError() . "\n";
                  $errorMsg.= "The Response message is: " . $error->getResponseBody() . "\n";
                  $output['success']     = false;
                  $output['error_message'] = $errorMsg;
              } 
              else 
              {
                  $orgDetails = array(
                    'userRef' => Session::get('userSessionId'),
                    'orgId' => Session::get('realmID'),
                    'companyName'=> $CompanyInfo->CompanyName,
                    'legalName' => $CompanyInfo->LegalName,
                    'companyAddr' => $CompanyInfo->CompanyAddr->Line1.' '.$CompanyInfo->CompanyAddr->Line2.' '.$CompanyInfo->CompanyAddr->Line3.' '.$CompanyInfo->CompanyAddr->Line4.' '.$CompanyInfo->CompanyAddr->Line5,
                    'city' => $CompanyInfo->CompanyAddr->City,
                    'country' => $CompanyInfo->CompanyAddr->Country,
                    'countryCode' => $CompanyInfo->CompanyAddr->CountryCode,
                    'postalCode' => $CompanyInfo->CompanyAddr->PostalCode,
                    'companyStartDate' => $CompanyInfo->CompanyStartDate,
                    'fiscalYearStartMonth' => $CompanyInfo->FiscalYearStartMonth,
                    'type' => 'quickbook'            
                  );
                  $output['success']          =   true;
                  $output['success_message']  =   $orgDetails;
                //Login::addOrgansationDetails($orgDetails);
              }         
            } 
            else
            {
              $errorMsg                 =   "Please connect with quickbooks again..";
              $output['success']        =   false;
              $output['error_message']  =   $errorMsg;
            }        
      }
      catch(Exception $e)
      {       
          $errorMsg = $e->getMessage();
          $output['success']        =   false;
          $output['error_message']  =   $errorMsg;
      }
      return redirect($this->url.'/user');  
      //return $output; die();
    }

    public function parseAuthRedirectUrl($url)
    {
        parse_str($url,$qsArray);
        return array(
            'code' => $qsArray['code'],
            'realmId' => $qsArray['realmId']
        );
    }

    //Cron for Updating Refresh and Access Token in Database for all users those are connected to Quickbooks.
    public function refreshQbToken($userId = null)
    { 
      $output = array();   
      try 
      {
        $qbTokenDetails = Helper::getUsersQuickbooksDetails($userId);   
        //Helper::echoprintcommand($qbTokenDetails)  ; 
        $detail         = Common::getAppDetails();   
        $client_id      = $detail[0]->client_id;
        $client_secret  = $detail[0]->client_secret; 
        $base_url       = $detail[0]->base_url;
        $auth_mode      = $detail[0]->auth_mode;  
        if(!empty($qbTokenDetails))
        {  
          foreach ($qbTokenDetails as $key => $value) 
          {              
            $user_id          = $value->user_id;
            $realmID          = $value->realm_id;
            $getAccessToken   = $value->access_token;
            $getRefreshToken  = $value->refresh_token;            
            $dataService = DataService::Configure(array(
                'auth_mode' => $auth_mode,
                'ClientID' => $client_id,
                'ClientSecret' =>  $client_secret,
                'accessTokenKey' =>  $getAccessToken,
                'refreshTokenKey' => $getRefreshToken,
                'QBORealmID' => $realmID,
                'baseUrl' => $base_url
            ));

            $OAuth2LoginHelper        = $dataService->getOAuth2LoginHelper();          
            $refreshedAccessTokenObj  = $OAuth2LoginHelper->refreshToken();
            $error                    = $OAuth2LoginHelper->getLastError();
            if($error) 
            {
              $errorMsg = "The Status code is: " . $error->getHttpStatusCode() . "\n";
              $errorMsg.= "The Helper message is: " . $error->getOAuthHelperError() . "\n";
              $errorMsg.= "The Response message is: " . $error->getResponseBody() . "\n";
              $output['success']     = false;
              $output['error_message'] = $errorMsg;           
            } 
            else 
            {
              //Refresh Token is called successfully
              $dataService->updateOAuth2Token($refreshedAccessTokenObj);
              $realmId           = $refreshedAccessTokenObj->getrealmId();
              $ngetAccessToken   = $refreshedAccessTokenObj->getAccessToken();
              $ngetRefreshToken  = $refreshedAccessTokenObj->getRefreshToken();                
              $error             = $dataService->getLastError();
              if ($error) 
              {
                $errorMsg = "The Status code is: " . $error->getHttpStatusCode() . "\n";
                $errorMsg.= "The Helper message is: " . $error->getOAuthHelperError() . "\n";
                $errorMsg.= "The Response message is: " . $error->getResponseBody() . "\n";
                $output['success']     = false;
                $output['error_message'] = $errorMsg;
              } 
              else 
              {                    
                $updatedTokn = array(
                    'user_id' => $user_id,
                    'realm_id' => $realmId,
                    'access_token'=> $ngetAccessToken,
                    'refresh_token' => $ngetRefreshToken,
                    'updated_at'=>date('Y-m-d H:i:s')
                );                 
                Common::qbUpdateToken($updatedTokn);  
                $output['success']     = true;
                $output['success_message'] = "Refresh token updated in database.";                 
              }              
            }
          }
        }   
        else
        {
          $output['success']       = false;
          $output['error_message'] = "Unable to refresh token in database, as user does not exist."; 
        } 
        return $output;
       exit;
      } 
      catch(Exception $e)
      {        
        $error1                   = $e->getMessage();
        $output['success']        = false;
        $output['error_message']  = $error1; 
        return $output;
        exit;
      }       
    }

    // Disconnect Quickbooks
    public function QuickbooksDisconnect()
    {
      try 
      {
        $allSession = Session::all();      
        $userSessionId = Session::get('userSessionId');
        if($userSessionId != "")
        {
          DB::table('quickbooks_details')->where('user_id', $userSessionId)->delete();
          Session::flush();
          //return ($this->url.'/user'); 
          $output['success']          = true;
          $output['success_message']  = "User disconnected from quickbooks successfully";          
        } 
      } 
      catch (Exception $e) 
      {
        $error1 = $e->getMessage();
        $output['success']        = true;
        $output['error_message']  = "Unable to disconnect user from quickbooks!";    
      } 
      return $output; exit();    
    }

    //Disconnect particular user from Quickbooks
    public function QuickbooksUserDisconnect(Request $request)
    { 
      try 
      {                 
        $userid = $request->input('userid');
        if($userid != "")
        {
          DB::table('quickbooks_details')->where('user_id', $userid)->delete();
          Session::flush(); 
          $output['success']          = true;
          $output['success_message']  = "Refresh token updated in database.";           
        }    
       } 
       catch (Exception $e) 
       {
          $error1 = $e->getMessage();
          $output['success']        = true;
          $output['error_message']  = "Unable to disconnect user from quickbooks!";      
        }  
        return $output; exit();     
    }
   
    public function getUsersListing(Request $request)
    { 
      $authUrl        =   $this->getAuthorizationURL();        
      $usersDetails   =   DB::table('users')->get();  
      return view($this->prefix.'/index',['title'=>'Quickbooks','usersDetails'=>$usersDetails,'qb_url'=>$authUrl]);
    }  

    public function commonQbConnectionCheck($userId)
    {  
      try 
      {   
          $returnService = Helper::qbConfigAndUserTokens($userId);     
          if(!empty($returnService))
          {    
            $error1 = ""; 
            try 
            {
               $dataService  = DataService::Configure($returnService);
            }
            catch(Exception $e)
            { 
              $error1 = $e->getMessage();
            }
            $error    = $dataService->getLastError();
            $response = array();
            if ($error || $error1)
            {
              $ermsg = "";
              $response['success']         = false;
              if ($error)
                $ermsg                     = $error->getResponseBody();
                $response['error_message'] = $ermsg.' '.$error1;
            }
            else
            {
              $response['success']     = true;   
              $response['success_msg'] = 'Connection established with quickbooks.';
              $response['dataService'] = $dataService;
            } 
          }
          else
          {
            $response['success']      = true;   
            $response['success_msg']  = 'This user qb connection is not established, please create qb connection';
            //Session::flush();              
          }
      } 
      catch (Exception $e)
      {
        $error1 = $e->getMessage();
        $response['success']          = false;
        $response['error_message']    = $error1;
           
      }  
      return $response;
      die();
      // redirect($this->url.'/user'); 
    }

    public function fetchQuickbooksAccounts($userId) 
    {      
      $checkConnection = $this->commonQbConnectionCheck($userId);   
      if(!empty($checkConnection))
      {  
        if( !$checkConnection['success'] )
        {
          echo $checkConnection['error_message'];
          return ($this->url.'/user');  
        }
        else
        {    
          $dataService      =   $checkConnection['dataService'];        
          //echo "<pre>"; print_r($dataService); echo "</pre>"; die;
         // $FetchAccounts    = $dataService->FindAll('Account');
          $FetchAccounts      = $dataService->query("SELECT * FROM Account ORDER BY ID DESC MAXRESULTS 5");
          $CompanyInfo = $dataService->getCompanyInfo();
          if(!empty($FetchAccounts))
          {
            //echo "<pre>"; print_r($CompanyInfo); echo "</pre>"; 
            echo "<pre>"; print_r($FetchAccounts); echo "</pre>"; 
          }
          else
          {
           // $this->refreshQbToken($userId);
            //echo "<pre>"; print_r($CompanyInfo); echo "</pre>"; 
            echo "<pre>"; print_r($FetchAccounts); echo "</pre>"; 
           // return redirect($this->url.'/user');
          }
        } 
      }
      else
      {
        return redirect($this->url.'/user');
      }     
    }

    public function createJournalEntryol()
    {
      $userId          =    1;
      $checkConnection =    $this->commonQbConnectionCheck($userId);   
      if(!empty($checkConnection))
      {  
        if( !$checkConnection['success'] )
        {
          echo $checkConnection['error_message'];
          return ($this->url.'/user');  
        }
        else
        {    
          $dataService    =   $checkConnection['dataService'];       
          // Prep Data Services
          $dataService->setLogLocation("/Users/hlu2/Desktop/newFolderForLog");
          $dataService->throwExceptionOnError(true);
          $theResourceObj = JournalEntry::create([
              "Line" => [
              [                
                "Description" => "nov portion of rider insurance",
                "Amount" => 100.0,
                "DetailType" => "JournalEntryLineDetail",
                "JournalEntryLineDetail" => [
                  "PostingType" => "Debit",
                  "Entity" => [
                     "Type" => "Vendor",
                     "EntityRef" => [
                         "value" => "1"                        
                     ]
                  ],
                  "AccountRef" => [
                      "value" => "58"                     
                  ],
                  "DepartmentRef" => [
                      "value" => "1"                     
                  ]
               ]
              ],
              [
                "Description" => "nov portion of rider insurance",
                "Amount" => 100.0,
                "DetailType" => "JournalEntryLineDetail",
                "JournalEntryLineDetail" => [
                  "PostingType" => "Credit",
                  "Entity" => [
                     "Type" => "Vendor",
                     "EntityRef" => [
                         "value" => "1"                        
                     ]
                  ],
                  "AccountRef" => [
                      "value" => "44"                      
                  ],
                  "DepartmentRef" => [
                      "value" => "1"                     
                  ]
                ]
              ]
            ]
          ]);

          //echo "<pre>"; print_r($theResourceObj); echo "</pre>"; die();
          $resultingObj = $dataService->Add($theResourceObj);
          $error = $dataService->getLastError();
          if ($error) {
             $errorMsg = "The Status code is: " . $error->getHttpStatusCode() . "\n";
              $errorMsg.= "The Helper message is: " . $error->getOAuthHelperError() . "\n";
              $errorMsg.= "The Response message is: " . $error->getResponseBody() . "\n";
              $output['success']     = false;
              $output['error_message'] = $errorMsg;
          }
          else {
              echo "Created Id={$resultingObj->Id}. Reconstructed response body:\n\n";
              $xmlBody = XmlObjectSerializer::getPostXmlFromArbitraryEntity($resultingObj, $urlResource);
              echo $xmlBody . "\n";
          }
        }
      }
    }

    public function createjournalEntry()
    {
      $userId          = 1;
      $checkConnection = $this->commonQbConnectionCheck($userId);   
      if(!empty($checkConnection))
      {  
        if( !$checkConnection['success'] )
        {
          echo $checkConnection['error_message'];
          return ($this->url.'/user');  
        }
        else
        { 
              $this->refreshQbToken($userId);   
              $dataService    =   $checkConnection['dataService'];       
              // Prep Data Services
              $dataService->setLogLocation("/Users/hlu2/Desktop/newFolderForLog");
              $dataService->throwExceptionOnError(true);      
              $variable =  array(    
                  'journalDate' => '09-06-2020',
                  'journalNumber' => '000',  
                  'description' => array('test1','test2'),
                  'amount' => array('100','100'),
                  'PostingType' => array('Credit','Debit'),
                  'customerType' => array('Vendor','Vendor'),
                  'customerID' => array(1,1,3,5),
                  'departmentRef' => array(1,4,55,87),
                  'accountReference' => array(94,256,77,554) 
              );
              //Helper::echoprintcommand($variable); die;
              $_POST          =   $variable;
              $DocNumber      =   $_POST['journalNumber'];          
              $journalDate    =   $_POST['journalDate'];         
              $description    =   $_POST['description'];   
              $amount         =   isset( $_POST['amount'] ) ? $_POST['amount'] : '0';
              $accountRef     =   $_POST['accountReference'];          
              $customerType   =   $_POST['customerType'];          
              $customerID     =   $_POST['customerID'];          
              $departmentRef  =   $_POST['departmentRef'];          
              $PostingType    =   $_POST['PostingType']; 
              $is             =   0;
              $array          =   array();
              for($is=0; $is < count($accountRef);$is++)
              { 
                $accountId  =   $accountRef[$is];
               // echo "SELECT * FROM Account where Id = '$accountId'";
                $Account    =   $dataService->query("SELECT * FROM Account where ID = '$accountId'");
                if(!empty($Account)){
                  $array['AccountExists'][] = $accountId;
                }
                else
                {
                  $array['AccountNotExists'][] = $accountId;
                }
                //Helper::echoprintcommand($Account);
              }
              if(empty($array['AccountNotExists']))  
              {       
                  $lineArray = array();
                  $i = 0;
                   for($i=0;$i < count($accountRef);$i++)
                   {  
                      $LineObj = Line::create([
                        "Id" => $i,
                        "LineNum" => $i,
                        "Description" => trim(strip_tags($description[$i])),
                        "Amount" => $amount[$i],
                        "DetailType" => "JournalEntryLineDetail",
                        "JournalEntryLineDetail" => [
                        "PostingType" => $PostingType[$i],
                        'JournalEntryEntity' => array(
                            'Type' => $customerType[$i],
                            'EntityRef' => array(
                                'value' => $customerID[$i] 
                            )
                        ),
                        "AccountRef" => [
                             "value" => $accountRef[$i]                     
                        ],
                        "DepartmentRef" => [
                            "value" => $departmentRef[$i]                     
                        ]
                      ]
                      ]);  
                      $lineArray[] = $LineObj; 
                      $amtt = ($i+1);                           
                   }         
                  $ddNm = $DocNumber.'100';        
                  $theResourceObj = JournalEntry::create([
                        "DocNumber" => $ddNm ,
                        "TxnDate"=> date('Y-m-d',strtotime($journalDate)),
                        "Line" =>  $lineArray,                             
                  ]);
                  $error1 = "";
                  try 
                  {
                    $resultingObj = $dataService->Add($theResourceObj);
                  }
                  catch(Exception $e)
                  {
                    $error1 = $e->getMessage();
                  }
                  $error = $dataService->getLastError();
                  $response = array();
                  if ($error || $error1)
                  {
                      $ermsg = "";
                      $response['success']    = false;
                      if ($error)
                      $ermsg          = $error->getResponseBody();
                      $response['error_msg']    = $ermsg.' '.$error1;
                  }
                  else
                  {
                    $response['success']      = true;
                    $response['qbInvoiceId']  = $resultingObj->Id;
                    $response['invoiceNo']    = $DocNumber;
                    $response['success_msg'] = 'Journal Added Successfully';
                  } 
              } 
              else
              {
                  $AccountNotExists           = implode(", ", $array['AccountNotExists']);
                  $response['success']        = false;             
                  $response['error_msg']      = "Account Ids ".$AccountNotExists.' does not exists in quickbooks.';
              } 
          return $response;
        }
      }
    }

    public function getJournalEntry()
    {
      $userId          = 1;
      $checkConnection = $this->commonQbConnectionCheck($userId);   
      if(!empty($checkConnection))
      {  
        if( !$checkConnection['success'] )
        {
          echo $checkConnection['error_message'];
          return ($this->url.'/user');  
        }
        else
        {    
          $dataService    =   $checkConnection['dataService'];
          $JournalEntry   =   $dataService->FindAll('JournalEntry');
          echo "<pre>"; print_r($JournalEntry); echo "</pre>"; 
        }
      }
      
    }

    // Fetch Departments from Quickbooks Account
    public function fetchQuickbooksDepartments() 
    {
      $qbTokenDetails                   = Helper::getUsersQuickbooksDetails();
      if(!empty($qbTokenDetails)) 
      {
        foreach ($qbTokenDetails as $kryss => $value) 
        {
          $userId          = $value->user_id;
          $checkConnection = $this->commonQbConnectionCheck($userId);   
          if(!empty($checkConnection))
          {  
            if( !$checkConnection['success'] )
            {
              echo $checkConnection['error_message'];
              return ($this->url.'/user');  
            }
            else
            {          
              $this->refreshQbToken($userId); 
              $checkDepartmentsExistsInDatabase = Helper::checkDepartmentsExistsInDatabase($userId);   
              if(empty($checkDepartmentsExistsInDatabase)) 
              {
                $dataService      =   $checkConnection['dataService']; 
                $FetchDepartments =   $dataService->FindAll('Department');          
                if(!empty($FetchDepartments))
                { 
                  //Helper::echoprintcommand($FetchDepartments); 
                  foreach( $FetchDepartments as $val)
                  {
                      $id                = $val->Id;
                      $Name              = $val->Name;
                      $SubDepartment     = $val->SubDepartment;
                      $ParentRef         = $val->ParentRef;
                      $FullyQualifiedNae = $val->FullyQualifiedName;
                      $Active            = $val->Active;
                      $DepartmentEx      = $val->DepartmentEx;
                      $Address           = $val->Address;
                      $CustomField       = $val->CustomField;
                      $AttachableRef     = $val->AttachableRef;
                      $domain            = $val->domain;
                      $status            = $val->status; 
                      $data           =   array('Name'=>$Name,"SubDepartment"=>$SubDepartment,"ParentRef"=>$ParentRef,"FullyQualifiedName"=>$FullyQualifiedNae, "Active"=>$Active,"DepartmentEx"=>$DepartmentEx,"Address"=>$Address,"CustomField"=>$CustomField,'AttachableRef'=>$AttachableRef,'domain'=>$domain,'status'=>$status,'Department_Id'=>$id,'user_id'=>$userId);
                     // Helper::echoprintcommand($data); 
                      DB::table('quickbooks_department')->insert($data); 
                   
                  }          
                } 
                else
                {
                  
                } 
              }
              else
              {
                $dataService      =   $checkConnection['dataService']; 
                $FetchDepartments =   $dataService->FindAll('Department');          
                if(!empty($FetchDepartments))
                { 
                  foreach( $FetchDepartments as $val)
                  {
                    $id                  = $val->Id;
                    if($id != '')
                    {
                      $Name              = $val->Name;
                      $SubDepartment     = $val->SubDepartment;
                      $ParentRef         = $val->ParentRef;
                      $FullyQualifiedNae = $val->FullyQualifiedName;
                      $Active            = $val->Active;
                      $DepartmentEx      = $val->DepartmentEx;
                      $Address           = $val->Address;
                      $CustomField       = $val->CustomField;
                      $AttachableRef     = $val->AttachableRef;
                      $domain            = $val->domain;
                      $status            = $val->status; 
                      $data           =   array('Name'=>$Name,"SubDepartment"=>$SubDepartment,"ParentRef"=>$ParentRef,"FullyQualifiedName"=>$FullyQualifiedNae, "Active"=>$Active,"DepartmentEx"=>$DepartmentEx,"Address"=>$Address,"CustomField"=>$CustomField,'AttachableRef'=>$AttachableRef,'domain'=>$domain,'status'=>$status,'Department_Id'=>$id,'user_id'=>$userId);
                      $insertRes = QuickbooksDepartment::updateOrCreate(['Department_Id' => $id,'user_id' => $userId], $data );                   
                    }
                  }          
                }
              }       
            } 
          }
          else
          {
            return redirect($this->url.'/user');
          } 
        } 
        echo "All Departments fetch according to Quickbooks companies.";
      }
      else
      {
        echo "Unable to fetch quickbooks departments! User is not connected with Quickbooks, first create connection with Quickbooks. ";
      }   
    } 
}
