<?php
namespace App\Http\Model;
use DB;
use Hash;
use Illuminate\Database\Eloquent\Model;

class QuickbooksDepartment extends Model
{
    protected $table = 'quickbooks_department';
    
    protected $guarded = [];	
}
