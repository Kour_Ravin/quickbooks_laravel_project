<?php
namespace App\Http\Model;
use DB;
use Hash;
use Illuminate\Database\Eloquent\Model;

class Common extends Model
{
    protected $table = 'client_app_details';

    public static function getAppDetails(){
		$resultVal =  DB::table('client_app_details')->select('*' )->get()->toArray();
		return $resultVal;
	}
    
    public static function getAllRowData($table, $data){
	    $result =  DB::table($table)->select('*')->where($data)->get();
	    return $result;
 	}

 	public static function getSingleRowData($table, $data){
	    $result =  DB::table($table)->select('*')->where($data)->first();
	    return $result;
  	}	
  
 	public static function qbUpdateToken($details){
	    $resultVal =  DB::table('quickbooks_details')->select('id' )->where(DB::raw('user_id'),$details['user_id'])->get();
	    if(count($resultVal) > 0){
	      $output = DB::table('quickbooks_details')->where('user_id',$details['user_id'])->update($details);
	    }
	    else{
	      $output = DB::table('quickbooks_details')->insert($details);
	    }
    	return $output;
 	}
 	
  	public static function getQbToken(){
	    $userID = Session::get('userSessionId');
	    $result = DB::table('quickbooks_details')->select('*')->where('user_id',$userID)->get();
	    return $result;
  	}

  	public static function addOrgansationDetails($details){
	    $resultVal =  DB::table('addorgansationdetails')->select('id' )->where(DB::raw('user_id'),$details['user_id'])->where(DB::raw('type'),$details['type'])->get();
	    if(count($resultVal) > 0){
	      $output = DB::table('addorgansationdetails')->where('user_id',$details['user_id'])->where(DB::raw('type'),$details['type'])->update($details);
	    }
	    else{
	      $output = DB::table('addorgansationdetails')->insert($details);
	    }
	    return $output;
  	}

}
