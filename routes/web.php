<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', 'QuickbooksController@getUsersListing');
Route::get('user', 'QuickbooksController@getUsersListing');

//QB

Route::any('/set-userid-session', 'QuickbooksController@setUserIdSession');
Route::any('/create-qb-invoice','QuickbooksController@createQbInvoice');
Route::any('/create-qb-journal','QuickbooksController@createJournalEntry');
Route::any('/create-qb-invoice2','QuickbooksController@createQbInvoiceStatic');
Route::any('/invoice/{id}','QuickbooksController@invoiceView');

Route::get('quickbooks-callback', 'QuickbooksController@QuickbooksCallBack');
Route::post('qb-disconnect', 'QuickbooksController@QuickbooksDisconnect');
Route::post('qb-user-disconnect', 'QuickbooksController@QuickbooksUserDisconnect');
Route::get('refresh-token', 'QuickbooksController@refreshQbToken');
Route::get('refresh-token/{usd}', ['uses' =>'QuickbooksController@refreshQbToken']);
Route::get('fetch-accounts/{id}', 'QuickbooksController@fetchQuickbooksAccounts');
Route::get('fetch-departments', 'QuickbooksController@fetchQuickbooksDepartments');
Route::get('get-journal/{id}', 'QuickbooksController@getJournalEntry');
