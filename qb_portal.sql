-- phpMyAdmin SQL Dump
-- version 4.7.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jun 04, 2020 at 11:21 PM
-- Server version: 5.7.28-0ubuntu0.16.04.2
-- PHP Version: 7.0.33-14+ubuntu16.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `qb_portal`
--

-- --------------------------------------------------------

--
-- Table structure for table `client_app_details`
--

CREATE TABLE `client_app_details` (
  `id` int(11) NOT NULL,
  `client_id` text NOT NULL,
  `client_secret` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `client_app_details`
--

INSERT INTO `client_app_details` (`id`, `client_id`, `client_secret`) VALUES
(1, 'ABQiKS152cVv77MAiWpHYOLOERW0FeKRVwsJStQV59gio7cICT', 'AGTbs6XZhJUXxkUkQOgvhLJ0HwK4KKeKp0Leew3F');

-- --------------------------------------------------------

--
-- Table structure for table `quickbooks_details`
--

CREATE TABLE `quickbooks_details` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `access_token` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `refresh_token` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `realm_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `quickbooks_details`
--

INSERT INTO `quickbooks_details` (`id`, `user_id`, `access_token`, `refresh_token`, `realm_id`, `created_at`, `updated_at`) VALUES
(1, 1, 'eyJlbmMiOiJBMTI4Q0JDLUhTMjU2IiwiYWxnIjoiZGlyIn0..DYjnjf-qiazi1YuXbMoDkg.2ltO6rwV3uA8V_UAZMF3MVZ_ehLtjA40bGulHDMJJIgH65HNDk2hi0dXhhZxCxPjOrrw3Q6Uqm7ZskpC9CZzWpvVWN5PQQXXLpbjS03-TNj1abVyvyFKwKKq-hcjp9b8et5sMu4ULh0YwvH9b7aHEDpQW1VWjV1aEFj3O-4h_hkN5WaMxSbxJvLKy1kT2-TSNAgzV5AGP0Y746WQ7D6fDJrGPhpXlnSiWp9whADzUI1eXX9dFRL1ijsUmBxvWRx4WZxl7rNAd3ZqRaohk2VC0ySfCdEa3J10mpV1NWyohZQTLLxQPBf5jOuXF_P9bQLOnWBzlEbhx-kIiDp2mIAWndTKaDjyve5zsYxtXLdQmokG40TjA8vHNm6cbCFqN8jNojbi9OwLD9oCdr6DtbMm2hfOCESmHuSu8s45eGO2N0ypYbgND5Q4Sr-DJc3SGH5xFnlWT9Bvf47KDW1B1BRSbOpt4nH2ytSF4R0gn8zZnUGeCys8tKJ-9bgZIleBRYjMvsfF18Qn3OUQyCLME2j_TpuFK07r7GpLrC7kPsWXaoKqcxfnYjBN4L5cB26I-KeZ7i6TGOzy1FZux8X2Kjl-ievBrnXbmUbe0JHYP6SRFNQjHpuhJpJgpGxjP639XOFgHu_zLgDh1-MHodfdZuh45okiCmGL_klgp_38wjx3xpvGJS1mmnqAq6TkYZDn5mUMecyyRktfZ23YRBtSppkaKtosLPfM2vDT-l3-ftgwe6Q.Lx0ZKRH4GsVDPABEPh5KUQ', 'AB11600018019nmhpCf76xOczeDa9AerjcZhTyP50Jk7Ov57yQ', '193514292435637', '2020-06-04 11:56:59', NULL),
(2, 2, 'eyJlbmMiOiJBMTI4Q0JDLUhTMjU2IiwiYWxnIjoiZGlyIn0..nHOJ24oDTB66WQ_BlVJ2oQ.oKpLmWdxNQr1P2nYyPjF3o2nbrZL40Rrm9O6zaUaQTZSjw7S93s87il7_vK2ZENloIOEp3HES9nU4XIcjISydhjegIMJJwYn3sC6dj30JImTTb_upo20D18hVg7flHdTRxhBq3m9g-jJ90O2V4XTZBw4P0hIn5TJyoLrA9kMo3CdWOww4-uqUHEy46i8aokq892oQlvM9QqMgJu-KYsYvQpOc0jd2aS-Ip4gOf4owp1tYfEmewYQneYtxHYqIb0x24YBpawPO-KQ9wq54PKqylYDI_X9bmQrvj9kfjTF6zXdU9Oc7IW2hNBLP8Lbdv-Batv4JNGCenUTFae_uMe6V7IjsWLvLlERfyDpf-J-FLJ4-gF9CS0gs9C4FKAX3Wr06zGzoAALLpagRrN8EIV9FOaseGF_VfeN16ExyGSm8gh_C-RzftyRHgopVa_7o6wvVQucf8bEwbaW-d3dwESiMS3tHIaFS5QgIccsUfY-uCceHLFtf2RHJDWoR3Fgtq0N0v33B8SfEvZQmcUpoEpqTN-IudNx0KmNTbOSRGUSGfcycXTH76ZByhegvwjsZIHbcORIs_GjmxLMMMjtzC1payFutInw8XBCRlAPZi7-YWdiT_AOBOfgnHJyLarYkXEY0zdDmWYaqVIZVo9YPsJtCLuywplrJR0IBqvpfY0hH2ju8y6WGchRdb-y53jakqkg9fRumbUfbOHeuoXMRSN7UMqON9cx9b6r75RSqePpNm8.x--NlNdZnKBrK7VST4ZGlg', 'AB11600018125MmVzbEienOrp9GKskGk4kn3dlLZpII8duFVAw', '193514292435637', '2020-06-04 11:58:45', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `user_email` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `user_name`, `user_email`) VALUES
(1, 'Tester', 'tester@gmail.com'),
(2, 'Dummy Test', 'dummy@gmail.com');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `client_app_details`
--
ALTER TABLE `client_app_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `quickbooks_details`
--
ALTER TABLE `quickbooks_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `client_app_details`
--
ALTER TABLE `client_app_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `quickbooks_details`
--
ALTER TABLE `quickbooks_details`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
